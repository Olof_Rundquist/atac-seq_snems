## ATAC-seq analysis guide on sigma
Hello! Welcome to my tutorial for ATAC-seq anlysis on the sigma cluster. This tutorial will walk you through at a very basic level how to setup ATAC-seq analysis on sigma. 
The tutorial is written to be understandable to people without prior knowledge of programing or unix based operating systems.
Therefore a crash course in both is given as well. The principles outlined in this tutorial also applies to other nges analysis pipelines. Good luck.

### Getting started 
Start by acquiring access to the sigma cluster by following the instructions on the snic webpage.
Connect to the cluster through ssh. On a Linux system this should be directly available in the terminal window. 
On windows I recommend you to download mobaxterm and log in through its terminal. Download the portable version since it bypasses the need for Administrator rights.
Alternatively use thinlinc.

Once logged in load the Anaconda module. Then create a conda virtual environment in your local folder. 
```bash
module load Python/3.6.3-anaconda-5.0.1-nsc1
conda create -n enviroment_name
source activate enviroment_name

#Set up channels to install packages
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```
Bioconda is conda repository for biological analysis tools. It contains many useful packages for HTseq analysis. Simply google bioconda to check their available packages.

Through out this tutorial we will be using many different types of software. 
Too install all of them at once use the below command.
It will take awhile install everything.

```bash
#Install conda pkgs.
conda install -y fastqc multiqc trim-galore bowtie2 samtools bedtools pip

#Install The Regular Genomics Toolbox
pip install cython numpy scipy
pip install RGT

#Install Genrich
module load git
git clone https://github.com/jsh58/Genrich.git
cd Genrich/
module load GCCcore/5.4.0
module load zlib/1.2.8
make
```

To make it so that your anaconda module is always loaded open .bashrc in your home directory and add the line `module load Python/3.6.3-anaconda-5.0.1-nsc1` to it.
While your at it for easy access to your ATAC-seq data also add it's location as an environment varibel: `export ATAC-seq_data_dir=<path_to_ATAC-seq_directory>`. 
You can now access your ATAC-seq data directory by simply typing `cd $ATAC-seq_data_dir`
To use Genrich add it as PATH varible: `PATH=$PATH:$HOME/Genrich/`.


### Setup your data directory.
It pays to have some organization.

```bash
mkdir ./ATAC-seq_data
mkdir ./ATAC-seq_data/data
```

move all of your raw data folders into the data folder.
Put all scripts in the directory above (ATAC-seq_data)
The assumed working directory for the turotial is the ATAC-seq_data/ folder.
Name this folder based on the ATAC-seq project and create similar separate folder for different sequencing projects.
When you are done with a project always keep the scripts around so that you can go back to them to check what you did. 
Also keep a README file for each project directory that outlines what it was and what was done with it.
If necessary also keep an installation guide for all of the programs.
This way you will know what script was used for each project.

### Pre-Alignment processing 

The first step of high throughput sequencing (HTseq) analysis is alignment of sequencing reads to the reference genome. 
However before we can do this we must ensure that the sequencing data is of good quality. To check sequencing quality Fastqc is usually used.
Fastqc takes a while to run so do not run it on the login node. Cluster computers like sigma uses a job system called SLURM to handle user job submissions. 
The key to efficient cluster usage is parallelization of inputs. There are several ways to achieve this and some of them are outlined on the SNIC website. The way I will outline is by using for loops.
For Fastqc the for loop won't be necessary since the parallelization is built into the program itself. Fastqc will check the number of reads, read quality, adapter content etc of your files and output a html report for each file. 
For each html report it will also output a zip file.
This report is quite useful but if you have many files it is very cumbersome to go through all of the reports. We will therefore compile all of the reports using multiqc. 
Open a document in a text editor (I recommend emacs or spyder) called fastqc.sh.

##### fastqc.sh
```bash
#!/bin/bash -l

#Below is the parameter for sbatch. To see the avilabel parmameter write sbatch --help on the command line.
#SBATCH -J fastqc # Name of the job
#SBATCH -t 02:00:00 # The wall time. If exceeded the script will be killed.
#SBATCH -n 32 # The number of cores to use. There is 32 cores per node.

source activate enviroment_name

#make the output directory for the reports
mkdir $2

#$1 is ./data/*/*fastq.gz 
#-t denotes the maximum number of files to process in parallel. Each uses about 250 mb of ram and we have 96 gb per node so 384 per node is max
fastqc -t 384 -o $2 $1  
# If you have less than 384 samples adjust the -t and SBATCH -n accordingly. If you try to open more than the available ram of threads fastqc will fail to start. 

#cd into output directory and run multiqc.
cd $2

multiqc *zip
```

So what is this? This is a bash script. The first line of a any given script is the shebang line. 
The shebang line tells the computer what language to use when reading the file and will vary with the programing language.
To submit the above script to the job queue write `sbatch fastqc.sh ./data/*/*fq.gz ./fastqc_reports`. 
This will run the script on all files ending with fq.gz in your ATAC-seq data directory and output the reports in a folder called fastqc_reports.

Change directory into the fastqc_reports directory.
Write `xdg-open multiqc_report.html`to open the report.
The important part of this report is the number of reads, the quality score and the adapter content.
If the adapter content is none and the average quality is close to 40 you can skip the trim-galore section. You should still read it as it introduces how to parallelize job scripts. 
The report will also give you information on duplicated sequences and repetitive sequences (kmers). 
ATAC-seq data will fail the quality check for both of these since we are sequencing very specific regions of the genome. 
This is in our case fine but if your data is for example RNA-seq this should not occur.

#### Adapter trimming and removal of low quality reads 
If you have sequencing adapters in the data you need to remove them before aligning your reads to the genome. Otherwise your alignment rate will be low.
There are many different programs for this but we will be using trim galore
The reason why we are using trim galore is because it maintains the structure of the input file. This is vital if you have pair-end sequencing data as otherwise your reads will no longer be paired.
Trim galore also sort out unpaired reads and bad quality reads. 
To speed up this step we are going to submit each sample as a separate job. This we do by creating a top level scripts that gives input to a low level script that runs the program.


Top:

##### trim_galore_submit.sh
```bash
#!/bin/bash -l

#How to write the find command depends on the look of your files.
#In order to save an output from a command to a variable in bash you use: `varibel=$(cmd)` as seen below.

#Find all of the sample directories
samples=$(find ./data/C* ./data/P* -prune -type d)

#give the files for each sample to trim galore. 
#Trim galore expects input on the form of forrward reverses forward reverse ...
#Simply find all files for each sample and give them as a group.
mkdir trimmed_galore
for i in $samples
do
    files=$(find $i/*/*.fq.gz -type f) 
    sbatch ./bin/trim_galore.sh "${files}" ${i##./*/} # ${i##./*/} = sample name 
done
# "${files}" sends the whole space delimted list as one argument (a string).
# take note that Bash processes things like {} and $ before the " " so "${files}" is read as the string of the varibel files. This is quite different from how f.ex python works. 
# Note if you omit the " " then the varible will be given as a list of argumnets rather than just one argument.

```
For the output files I have used %% and ##, this is something known as parameter expansion. 
What it does is to match what comes after ## or %% to the either the start (##) or the end (%%) of the string and remove anything that matches. In this case "./*/" = dot slash followed by any character and then slash. Removes ./data/

Bottom:

##### trim_galore.sh

```bash
#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 32
#SBATCH -J Trim_galore

files=${1} # space delimted list of input files, ${1} = input argument 1. Input zero is the script.
name=${2}

mkdir trimmed_galore/${name}
trim_galore -o trimmed_galore/${name} --nextera --fastqc --paired --retain_unpaired ${files} 

```
The above script will take pairs of sequencing read files and submit them to trim galore for clean up. 

You should now have a directory called trim galore with one folder per sample and the clean files in said folders.


### Alignment
Now we can align the reads to the reference genome.
There are multiple sequence aligners to use for ATAC-seq. The most popular one is bowtie2.
In order to align your reads to a reference genome you need a reference genome.
There is two ways to get a refrence genome:
Either you download the fasta file of your genome of interest (for human, hg38) from ensemble or refseq and index it with bowtie. This takes a while. 
Or you can download a pre-indexed version from the bowtie web page using wget.
```bash
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/GRCh38_no_alt.zip
```
Create an index directory for bowtie and unzip the index to it. It is recommended that you create this directory in your project folder since the index is quite large.
Then create a path to the index in your .bashrc file like you did for the ATAC-seq directory: `export hg38_bowtie_index=/path_to_bowtie_index` and do source .bashrc.
I would recommend to have a dedicated directory for reference genomes in the project folder.
The file to export is the file with the .fna ending.
And now align. Note that we are now going to chain multiple scripts together.

##### Align.sh

```bash
#!/bin/bash -l 

#Runs the alignment and sort the files afterwards.

#Find The sample directories
samples=$(find ./data/trimmed_galore/C* ./data/trimmed_galore/P* -prune -type d)
# This finds the forward and reverse files and gives them to bowtie. 
mkdir aligned
for i in $samples
do
    f=$(find $i/*val_1.fq.gz -type f) # Forward
    r=$(find $i/*val_2.fq.gz -type f) # Reverse
    unp=$(find $i/*unpaired*gz -type f) # Unpaired reads 
    sbatch ./bin/bowtie_run.sh "${f}" "${r}" "${unp}" ./aligned/${i##./*/} ${hg38_bowtie_index}
done


```

##### bowtie.sh

```bash
#!/bin/bash

#SBATCH -J bowtie2
#SBATCH -t 04:00:00
#SBATCH -N 1
#SBATCH -n 32

forward=$(echo ${1} | tr " " ",") # Creates comma delimited list of forward read files
reverse=$(echo ${2} | tr " " ",") # Creates comma delimited list of reverse read files
unpaired=$(echo ${3} | tr " " ",") # Creates comma delimited list of unpaired read files
ref_index=${5} # Reference genome.
out=${4}

bowtie2 -p 32 -k 10 --very-sensitive -x $ref_index -1 ${forward} -2 ${reverse} -U ${unpaired} -S ${out}.Aligned.sam --met-file ${out}.bowtie.metrics.txt

sbatch sam_to_bam.sh ${out}.Aligned.sam

```

Bowtie takes input as comma delimited list files with the forward, reverse and unpaired files seperate. Note that at the end of the script we are sending the next step of the pipeline on. 
This is very good since it means that once all the script are in order the pipeline will essentially run automatically from here.
One can write the next script into the script above but I recommend to keep dedicated scripts separate so that if things go wrong it is clear where it went wrong. 

##### sam_to_bam.sh

```bash
#!/bin/bash -l

#SBATCH -J Sam_to_bam
#SBATCH -n 1
#SBATCH -t 02:00:00
#SBATCH --mem=6144 # 6gb

sam=${1}

samtools view -bS $sam > ${sam%%.sam}.bam
samtools sort -m 6G -n ${sam%%.sam}.bam > ${bam%%.*}.sorted.bam
samtools index ${sam%%.*}.sorted.bam

rm $sam ${sam%%.*}.rmdup.bam # clean up
```

### Peak calling

To call peaks we will use Genrich. Genrich is a recently released peak caller recommended by the Harvard ATAC-seq guidelines. Previously they recommend MACS2.
Genrich can be found here on github: https://github.com/jsh58/Genrich

Peaks will be called acroos all samples simultanously to generate a consensus peak set. 
In our case we had a case controll study looking at T-cell activation so we will also call differential peaks between the two conditions.
These differential peaks will be the focus of the later footprinting and TF analysis while the total peak set will be used as the background.

##### genrich_input.sh

```bash
#!/bin/bash -l

P_A=$(find P*A_*bam -print0 | tr "\0" ",")
P_U=$(find P*U_*bam -print0 | tr "\0" ",")
C_A=$(find C*A_*bam -print0 | tr "\0" ",")
C_U=$(find C*U_*bam -print0 | tr "\0" ",")


#Comparing all controlls vs all patients.
echo submited genrich diff_dynamic "${P_A,$P_U}" "${C_A},${C_U}"
sbatch genrich_diff.sh "${P_A},${P_U}" "${C_A},${C_U}" Disease.bed

echo submited genrich diff_dynamic "${P_A},${C_A}" "${P_U},${C_U}"
sbatch genrich_diff.sh "${P_A},${C_A}" "${P_U},${C_U}" Activation.bed

#All samples vs the null model background. Constitutes all detected open regions in the dataset.
sbatch genrich_run.sh "${P_A},${P_U},${C_A},${C_U}" all_peaks.bed

```
##### genrich_run.sh
```bash
#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem=65496 # =64 gb of ram

bams=$1
out=$2
Genrich -t $bams -o $out -j -y -r -e chrM -v 2> ${out%%bed}err
```
##### genrich_diff.sh
```bash
#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 1
#SBATCH -t 20:00:00
#SBATCH --mem=65496 # =64 gb

patients=$1
controls=$2
out=$3

Genrich -t $patients -c $controls -o $out -j -y -r -e chrM -v 2> ${out%%bed}err
```

### Goterm anlysis of related genes with HOMER

An easy anlysis to get a rougth picture of what genes that are affected by the experimental conditions is to annotate the peaks to the nearest gene and do a go term analysis. 
This can be done with the Homer package:

##### annotate_homer_input.sh
```bash
#!/bin/bash -l

for i in *bed
do 
    sbatch annotate_homer.sh $i
done
```
##### annotate_homer.sh
```bash
#!/bin/bash -l

#SBATCH -t 00:45:00
#SBATCH -n 1

#This was a forloop
i=$1

mkdir ./homer/${i%%.bed}_go/
mkdir ./homer/${i%%.bed}_genomeOntology/
annotatePeaks.pl $i hg38 -size given -annStats ./homer/${i%%bed}annstat -go ./homer/${i%%.bed}_go/ -genomeOntology ./homer/${i%%.bed}_genomeOntology/ > ./homer/${i%%.bed}_annotated.bed
```

### Footprinting and motif matching

To start the footprinting first setup the rgtdata folder. The original instruction can be found here: http://www.regulatory-genomics.org/rgt/rgt-data-folder/
The rgt data folder takes up about 18 gb so move it to your project dir and symlink it to it's original location.

```bash
cd ~/rgtdata
python setupGenomicData.py --hg38
```

Now setup the motifs and logos data. The logos are the visual representation of the motifs (PWMs). Instructions here: 

```bash
cd ~/rgtdata
python setupLogoData.py --all
```

This will generate PWMs and setup logos data for the databases HOCOMOCO, Jaspar-vertabrates and uniprobe. We will most likley use either Jaspar or HOCOMOCO.

Done.

### Usage

The below analysis was based on this: https://www.regulatory-genomics.org/hint/tutorial/

#### Footprinting with rgt-hint

Footprinting with rgt-hint is very straight forward and very fast. Footprinting on 20 000 - 30 000 peaks can successfully be run on one core in less than 5 minutes per sample.
Note that for higher number of peaks additional memory and time will be needed . Ex 600 000 peaks uses 10 gb of ram and takes about 6h per sample.
Footprinting with rgt-hint is not multi-threaded.
Also note that rgt hint produces an extreme number of footprints since it does not make a cutoff.

To run rgt for all samples on a set of peaks do the following: 
Do this for all interseting sets and the background regions. 

##### rgt_hint_input.sh 
```bash
#!/bin/bash -l

# Use bam files with marked duplicates. Generate these with either `samtools rmdup` or `picard`. 
# Ideally we should use files filtered and deduplicated in the same way as Genrich does but there does not seem to be an option for Genrich to output the deduplicated bam file.

activation=Activation.bed # The peak set to run on.
all_peaks=all_peaks.bed

for i in *.rmdup.bam
do
sbatch rgt_hint_run.sh $i $activation
done 

for i in *.rmdup.bam
do
sbatch --mem=20000 -t 08:00:00 rgt_hint_run.sh $i $all_peaks
done 


```

##### rgt_hint_run
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 00:30:00

bam=$1
bed=$2

out=${1##../*/} # Crops input to only the file name.
rgt-hint footprinting --organism hg38 --atac-seq --paired-end --output-prefix=${out%%.*}_${bed%%.*}_rgt_fp ${bam} ${bed} 
```

#### Match to motifs

Next we will match all of our footprints to motifs.
The default database is jaspar veterbrates. If you want to use hocomoco that has more TFs you can't do that through the command line and instead have to set it in the rgt user config file.

First open your rgtdata/data.config.user file and copy in the below lines to the end of the file:

```
[MotifData]
repositories: hocomoco
```
This will make it so that all future motif matches are with hocomoco. 

So now matching:
##### rgt_motif_match_input.sh
```bash
#!/bin/bash -l

for i in *activation_rgt_fp.bed
do
    sbatch rgt_motifanalysis_run.sh $i
done

for i in *all_peaks_rgt_fp.bed
do
    sbatch --mem=12286 -t 03:00:00 rgt_motifanalysis_run.sh $i
done
```

##### rgt_motif_match_run.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 01:00:00

rgt-motifanalysis matching --organism hg38 --input-files $1

```

This will produce two mpbs (motif predicted binding site) files for each sample in the ./match directory

#### TF Enrichment
Looking for enriched TFs in the motif matched files for each sample.

##### rgt_motifanalysis_enrichment_input.sh
```bash
#!/bin/bash -l

background=($(find *all_peaks*fp*bed))
activation=($(find *activation*fp*bed))

for index in ${!activation[*]}
do
	sbatch rgt_motifanalysis_enrichment_run.sh ${background[$index]} ${activation[$index]}
done
```
##### rgt_motifanalysis_enrichment_run.sh
```bash
#!/bin/bash -l

#SBATCH -j rgt-enrichment
#SBATCH --mem=32748 # This uses up to 30 gb of ram.
#SBATCH -t 04:00:00

background=$1
activation=$2
rgt-motifanalysis enrichment --organism hg38 $background $activation

```

We now have an enrichment score for all TFs in the activation regions compared to the background.  

#### Differential analysis

Annother thing one can do is to look att differential TF activity. 

##### rgt_hint_differential_input.sh

```bash
#!/bin/bash -l

A_reads=($(find ../../*A_*.rmdup.bam))
U_reads=($(find ../../*U_*.rmdup.bam))

A_mpbs=($(find ./match/*A_*activation*mpbs.bed))
U_mpbs=($(find ./match/*U_*activation*mpbs.bed))

for index in ${!A_reads[*]}
do
	sbatch rgt_hint_differential_run.sh ${U_reads[$index]} ${A_reads[$index]} ${U_mpbs[$index]} ${A_mpbs[$index]}
done
```

##### rgt_hint_differential_run.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 8 # Rgt-hint differential can not efficiently use more than 8-12 threads. I have tested this.
#SBATCH -t 02:00:00

reads1=$1
reads2=$2
mpbs1=$3
mpbs2=$4

out=${mpbs1##./*/}
rgt-hint differential --output-profiles --bc --organism hg38 --mpbs-file1 $mpbs1 --mpbs-file2 $mpbs2 --reads-file1 $reads1 --reads-file2 $reads2 --condition1 Act --condition2 UAct --nc 8 --output-location ./${out%%U*.bed}_differential 

```

This will produce a differential activation profile for evry sample. These profiles could then be compared to determine what Tfs define each sample group and if there is a difference in patients and controlls.








