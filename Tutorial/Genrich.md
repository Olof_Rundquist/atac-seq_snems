## What is Genrich

Genrich is a recently released peak caller recommended by the Harvard ATAC-seq guidelines. This was updated in January I think. Previously they recommend MACS2.
Genrich can be found here on github and is very easy to install. https://github.com/jsh58/Genrich

### So why are we testing it?

Genrich was designed with the intent of creating a program that through one command runs everything post alignment for peak calling. 
This means that Genrich comes with several interesting features that you normally have to do with separate tools if you are using MACS2. 
All of this information can be found here: https://informatics.fas.harvard.edu/atac-seq-guidelines.html but in brief: 
Genrich removes duplicate reads, allows you to disregard whole chromosomes through the -e option, calculates genome size so you don't have to etc.

Outside of efficiency there are several other reasons to use Genrich. 
Hiseq reads as they are short and in the case of ATAC-seq from very specific regions are likely to map equally well to multiple parts of the genome. 
Normally these secondary alignments are disregarded in Peak calling but Genrich takes them into account. 
It's worth noting though that we are not currently using this feature as you have to tell Bowtie (the aligner) to report these secondary alignments through the option -k. Harvard recommends to set k to 10. 
This was not in the old guidelines that I partially based my analysis on.
By default if Bowtie finds a read that maps to several place in the genome equally well one of those places are chosen at random and given a low score. 
This will in later stages of the analysis cause the read to be disregarded entirely and the information will be lost.
Other reasons are that the options of MACS2 does not really fit ATAC-seq. 
Most recommendations for MACS2 recommends setting specific exit (insert) sizes and modeling options. 
However the mode that allows these options disregards all reverse reads as it was designed for single end data. Also you are just guessing the insert size here.
The other option is to set MACS2 to BAMPE mode but this mode does not let you define any peak calling options. You can give the options still but the program disregards them and does not tell you about it.
Genrich also lets you call peaks from multiple samples collectively and calculates peaks by combining P-values from multiple samples through the fisher method. 
This is also available in MACS but the way it combines p-values is different.
To consider multiple replicates with MACS2 usually peaks are called separately for each sample and a tool called IDR (irreproducible discovery rate) is used to compare samples to each other. This tool however only allows pairwise comparisons so using it with many samples is very impractical.
Finally with Genrich it is also possible to set an entire set of samples as background allowing for differential peak detection between samples. 
This however is also available in MACS2.

### So what have I used it for?

I have used Genrich for all groups and pairwise comparison. I have also called peaks for all groups separately without control samples. 
In this case samples are compared towards a log-normal random distribution.

genrich_input
```bash
#!/bin/bash -l

P_A=$(find P*A_*bam -print0 | tr "\0" ",")
P_U=$(find P*U_*bam -print0 | tr "\0" ",")
C_A=$(find C*A_*bam -print0 | tr "\0" ",")
C_U=$(find C*U_*bam -print0 | tr "\0" ",")

#The echo statements it to keep track of which job is which.
echo submited genrich $P_A
sbatch genrich_run.sh ${P_A} P_A.bed
echo submited genrich $P_U
sbatch genrich_run.sh ${P_U} P_U.bed
echo submited genrich $C_A
sbatch genrich_run.sh ${C_A} C_A.bed
echo submited genrich $C_U
sbatch genrich_run.sh ${C_U} C_U.bed

#Comparing within the groups.
echo submited genrich diff $P_A $P_U
sbatch genrich_diff.sh ${P_A} ${P_U} P_diff.bed
echo submited genrich diff $C_A $C_U
sbatch genrich_diff.sh ${C_A} ${C_U} C_diff.bed
echo submited genrich diff $P_A $C_A
sbatch genrich_diff.sh ${P_A} ${C_A} PC_A_diff.bed
echo submited genrich diff $P_U $C_U
sbatch genrich_diff.sh ${P_U} ${C_U} PC_U_diff.bed

#Comparing all controlls vs all patients. Takes awhile.
echo submited genrich diff_dynamic "${P_A,$P_U}" "${C_A},${C_U}"
sbatch -n 32 -t 12:00:00 genrich_diff.sh "${P_A},${P_U}" "${C_A},${C_U}" Dynamic_Response.bed

echo submited genrich diff_dynamic "${P_A},${C_A}" "${P_U},${C_U}"
sbatch -n 32 -t 12:00:00 genrich_diff.sh "${P_A},${C_A}" "${P_U},${C_U}" Activation.bed

#For all samples?
sbatch genrich_run.sh -n 32 -t 12:00:00 "${P_A},${P_U},${C_A},${C_U}" All_Peaks_all_samples_Genrich.bed
```

genrich_run.sh
```bash
#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 16 
#The -n 16 is mostly due to memory usage. Genrich is not multi threaded but uses more than 30gb of ram.
#SBATCH -t 8:00:00

bams=$1
out=$2

Genrich -t $bams -o $out -j -y -r -e chrM -v 2> ${out%%bed}err
```
genrich_diff.sh
```bash
#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 16
#SBATCH -t 20:00:00

patients=$1
controls=$2
out=$3

Genrich -t $patients -c $controls -o $out -j -y -r -e chrM -v 2> ${out%%bed}err
```

Table of number of peaks per condition:
See the Genrich input for what comparison the file corresponds to
   
    329799 P_A.bed
    322925 P_U.bed
    210761 C_A.bed
    225014 C_U.bed
    8262 P_diff.bed
    5446 C_diff.bed
    15409 Activation.bed
    293442 Dynamic_Response.bed
    209862 PC_A_diff.bed
    208586 PC_U_diff.bed

To see what peaks are shared and not. See upsetR plot.
The issue here is interpretation.
For an example the reason why there are more peaks in patients than controls is partially because there are more patients than controls so there are more p_value to combine in order to reach significance.
There are very few peaks for the activation compared to patients vs controlls. This is consistent with Julias approch but is unexpected. How much of the patients vs controls effect is just batch effect?
The shear number of peaks also confounds analysis as there is simply to much noise. 
Genrich however reports a p-values and q-values for every peak so the cutoff can be adjusted as we see fit to reduce the number of peaks.

Anyway post this there are also several other problems that mainly concerns footprinting and should probably be asked to David. 
With wellington it is not possible to call footprints from multiple samples at once. 
We have tried to solve this by calling footprints with Bam files created by combining all samples of a certain group and look at diffrences. This however invaldates the replicate information so is not ideal.
The alternative is to call footprints for all samples seperatley and create some kind of combined bed file based on if a footprint occurs in many samples and its FDR value. Similar to the consensus peaks approach that Andreas created (he was only looking at the overlap of peaks though and not FDR).
Here Genrich  gave me an Idea: 

	1. Create footprints for all samples for a set of peaks. Maybe lower the P-value cutoff to capture as many footprints as possible.
	
	2. In the footprints directory created there is folder were the p-value of all detected footprints are reported with a cutoff. 
	Take the lowest cutoff file from each sample and like Genrich use the fisher method to combine P-values and pick all footprints below a suitable cutoff. 

So from what peakset should we call footprints? Well this should probalby be asked to David but I would create a peak set from all samples with Genrich and call footprints on that one.
Then the fisher method would be applied as above to get all footprints that shows up in a certain group.
The bed files from the group comparisons from Genrich could then be used to tell if the peak that the footprint is in is also associated with that group. 
Just note that we will probably need to set a more stringent cutoff than the base Genrich one as it discovers way to many peaks and running all of them would burn months worth of cluster time allocation.
We however do not need to rerun anything for this as one can merely subset the output Genrich file based on P or q-value. 
FOR EX: 15000 peaks for one sample on 32 cores with full parallelization takes 9h for wellington.
So if we had lets say 300 000 peaks it would take at least 180h per sample. 180 hours * 32 cores * 84 samples = 483 840 core hours. As a result it is not really reasonable to footprint for more than 50 000 peaks.
