## Where_to_find_files_and_what_is_in_them

I have runed the RGT toolbox for everything we discussed now I think.

All the results are in the fotprinting folder inside the Genrich folder:

/proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/Genrich/footprinting

### Footprints
We have footprints both for the activation associated peaks and all peaks.
A through explanation of all things footpritns can be found in Gusamo's (the author of rgt-hint) phd thesis

http://publications.rwth-aachen.de/record/681125/files/681125.pdf?subformat=pdfa

I recommend to use this for the math symbols:
https://en.wikipedia.org/wiki/List_of_mathematical_symbols

#### File explanantion:

The footprint files are not complicated. 

Header: CHR, Start, End, CHR:Start-End, Read counts over fp, strand.

#### Motif match files
In the ./match directory.
There are two files per sample , one for the the activation peaks and one for all peaks. The one for all peaks was used as background for the enrichment test.

##### File explanantion:
One TF is reported for each footprint in the footprint file followed by a score for the alignment between the reported TFs motif and the footprint. 
This is reported as the bitscore of the alignment. See the Stat_scores.pdf file for an explanation of what the bitscore is.

Header: CHR, Start, End, Matching TF, Bitscore, strand

#### Enrichment results:
In the ./enrichment directory.

Arranged as directories with one directory per sample.The directory contains a html report of the enrichment, the enrichment as a table, and the pbs_env file whitch list the foreground against the background.

##### File explanantion:
Only the fulltest statistics files are really intresting

fulltest_statistics.[html and txt]: Contains the enrichment results in html and tab-separated text format. 
The results include, in order, the motif name, enrichment p-value, corrected enrichment p-value, Fisher’s exact test A value, Fisher’s exact test B value, Fisher’s exact test C value, Fisher’s exact test D value, foreground frequency (A/(A+B)), background frequency (C/(C+D)) and a list of genes that were matched to the regions in which that motif occurred separated by comma.

#### Differential TF activity results.
This is the somewhat experimental part of the analysis. Here I have compared the TF activity between activated and unactivaqted for all samples in the activation assocaited peaks. 
I will do this for all peaks in due time. The memory requirment for it however means that it will take a while. 
For each sample we here get what TFs are differentilly active between activated and unactivated and whitch TF "defines" the activated vs the unactivated state.
This is reported on a per sample basis. Note that the activty score reported is not on a per footprint basis and instead is meant to describe the activty of the TF as whole within that sample.

The files are in the directories marked sample_differential.

##### Each directory contains:

###### UAct_Act_factor.txt
Just a normalisation factor

###### UAct_Act_statistics.pdf
A graphical represenation of the results marking TFs associated with each condition. 
###### UAct_Act_statistics.txt
The result in text. 

Header: in detail:
```
    1. MOTIF: The TF.
    2. NUM: The number of time the TF was reported.
    3. Protection_Score_UAct: The protection score in condition 1
        a. Protection score = The protection score is a representation of how tightly the TF is binding.
        It is based on the footprint depth and essentily means that the tighter 
        the TF binds the more protection it provides against Tn5 trasnpostion 
        and the deeper the footprint will be.
    4. Protection_Score_Act: Same but for condition 2
    5. Protection_Diff_UAct_Act: The difference betwenn the scores. (Act - UAct)
    6. TC_UAct: A ranked based measurment of the number of reads spanning the reported footprints in condition 1
    7. TC_Act: A ranked based measurment of the number of reads spanning the reported footprints in condition 2
    8. TC_Diff_UAct_Act: TC_UAct - TC_Act
    9. TF Activty:  Protection_Diff_UAct_Act + TC_Diff_UAct_Act (if both negative, - instead)
```

###### Lineplots directory
Contains the avarage ATAC-seq signal around the reported motifs as PWMs in condtion 1 and 2. Useful for visulasation. 
I also outputed the profiles in the form of text but I am not sure how to use them  
