## ATAC-seq analysis guide on sigma
Hello! Welcome to my tutorial for ATAC-seq anlysis on the sigma cluster. This tutorial will walk you through at a very basic level how to setup ATAC-seq analysis on sigma. 
The tutorial is written to be understandable to people without prior knowledge of programing or unix based operating systems.
Therefore a crash course in both is given as well. The principles outlined in this tutorial also applies to other nges analysis pipelines. Good luck.

### Getting started 
Start by acquiring access to the sigma cluster by following the instructions on the snic webpage.
Connect to the cluster through ssh. On a Linux system this should be directly available in the terminal window. 
On windows I recommend you to download mobaxterm and log in through its terminal. Download the portable version since it bypasses the need for Administrator rights.
Alternatively use thinlinc.

Once logged in load the Anaconda module. Then create a conda virtual environment in your local folder. 
```bash
module load Python/3.6.3-anaconda-5.0.1-nsc1
conda create -n enviroment_name
source activate enviroment_name

#Set up channels to install packages
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```
Bioconda is conda repository for biological analysis tools. It contains many useful packages for HTseq analysis. Simply google bioconda to check their available packages.

Through out this tutorial we will be using many different types of software. 
Too install all of them at once use the below command.
It will take awhile install everything.

```bash
#Install conda pkgs.
conda install -y fastqc multiqc trim-galore bowtie2 samtools bedtools pip bioconductor-rsubread

#Install The Regular Genomics Toolbox
pip install cython numpy scipy
pip install RGT

#Install Genrich
module load git
git clone https://github.com/jsh58/Genrich.git
cd Genrich/
module load GCCcore/5.4.0
module load zlib/1.2.8
make
```

To make it so that your anaconda module is always loaded open .bashrc in your home directory and add the line `module load Python/3.6.3-anaconda-5.0.1-nsc1` to it.
While your at it for easy access to your ATAC-seq data also add it's location as an environment varibel: `export ATAC-seq_data_dir=<path_to_ATAC-seq_directory>`. 
You can now access your ATAC-seq data directory by simply typing `cd $ATAC-seq_data_dir`
To use Genrich add it as PATH varible: `PATH=$PATH:$HOME/Genrich/`.


### Setup your data directory.
It pays to have some organization.

```bash
mkdir ./ATAC-seq_data
mkdir ./ATAC-seq_data/data
```

Move all of your raw ATAC-seq data folders into the data folder. 
For now the organization is a bit of the mark. This will be corrected for the final version of the tutorial.

The assumed working directory for the turotial is the ATAC-seq_data/ folder.
Name this folder based on the ATAC-seq project and create similar separate folder for different sequencing projects.
When you are done with a project always keep the scripts around so that you can go back to them to check what you did. 
Also keep a README file for each project directory that outlines what it was and what was done with it.
If necessary also keep an installation guide for all of the programs.
This way you will know what script was used for each project.

### Pre-Alignment processing 

The first step of high throughput sequencing (HTseq) analysis is alignment of sequencing reads to the reference genome. 
However before we can do this we must ensure that the sequencing data is of good quality. To check sequencing quality Fastqc is usually used.
Fastqc takes a while to run so do not run it on the login node. Cluster computers like sigma uses a job system called SLURM to handle user job submissions. 
The key to efficient cluster usage is parallelization of inputs. There are several ways to achieve this and some of them are outlined on the SNIC website. The way I will outline is by using for loops.
For Fastqc the for loop won't be necessary since the parallelization is built into the program itself. Fastqc will check the number of reads, read quality, adapter content etc of your files and output a html report for each file. 
For each html report it will also output a zip file.
This report is quite useful but if you have many files it is very cumbersome to go through all of the reports. We will therefore compile all of the reports using multiqc. 
Open a document in a text editor (I recommend emacs or spyder) called fastqc.sh.


##### fastqc.sh
```bash
#!/bin/bash -l

#Below is the parameter for sbatch. To see the avilabel parmameter write sbatch --help on the command line.
#SBATCH -J fastqc # Name of the job
#SBATCH -t 02:00:00 # The wall time. If exceeded the script will be killed.
#SBATCH -n 32 # The number of cores to use. There is 32 cores per node.

source activate enviroment_name

#make the output directory for the reports
mkdir $2

#$1 is ./data/*/*fastq.gz 
#-t denotes the maximum number of files to process in parallel. Each uses about 250 mb of ram and we have 96 gb per node so 384 per node is max
fastqc -t 384 -o $2 $1  
# If you have less than 384 samples adjust the -t and SBATCH -n accordingly. If you try to open more than the available ram of threads fastqc will fail to start. 

#cd into output directory and run multiqc.
cd $2

multiqc *zip
```

So what is this? This is a bash script. The first line of a any given script is the shebang line. 
The shebang line tells the computer what language to use when reading the file and will vary with the programing language.
To submit the above script to the job queue write `sbatch fastqc.sh ./data/*/*fq.gz ./fastqc_reports`. 
This will run the script on all files ending with fq.gz in your ATAC-seq data directory and output the reports in a folder called fastqc_reports.

Change directory into the fastqc_reports directory.
Write `xdg-open multiqc_report.html`to open the report.
The important part of this report is the number of reads, the quality score and the adapter content.
If the adapter content is none and the average quality is close to 40 you can skip the trim-galore section. You should still read it as it introduces how to parallelize job scripts. 
The report will also give you information on duplicated sequences and repetitive sequences (kmers). 
ATAC-seq data will fail the quality check for both of these since we are sequencing very specific regions of the genome. 
This is in our case fine but if your data is for example RNA-seq this should not occur.

#### Adapter trimming and removal of low quality reads 
If you have sequencing adapters in the data you need to remove them before aligning your reads to the genome. Otherwise your alignment rate will be low.
There are many different programs for this but we will be using trim galore
The reason why we are using trim galore is because it maintains the structure of the input file. This is vital if you have pair-end sequencing data as otherwise your reads will no longer be paired.
Trim galore also sort out unpaired reads and bad quality reads. 
To speed up this step we are going to submit each sample as a separate job. This we do by creating a top level scripts that gives input to a low level script that runs the program.


Top:

##### trim_galore_submit.sh
```bash
#!/bin/bash -l

#How to write the find command depends on the look of your files.
#In order to save an output from a command to a variable in bash you use: `varibel=$(cmd)` as seen below.

#Find all of the sample directories
samples=$(find ./data/C* ./data/P* -prune -type d)

#give the files for each sample to trim galore. 
#Trim galore expects input on the form of forrward reverses forward reverse ...
#Simply find all files for each sample and give them as a group.
mkdir trimmed_galore
for i in $samples
do
    files=$(find $i/*/*.fq.gz -type f) 
    sbatch ./bin/trim_galore.sh "${files}" ${i##./*/} # ${i##./*/} = sample name 
done
# "${files}" sends the whole space delimted list as one argument (a string).
# take note that Bash processes things like {} and $ before the " " so "${files}" is read as the string of the varibel files. This is quite different from how f.ex python works. 
# Note if you omit the " " then the varible will be given as a list of argumnets rather than just one argument.

```
For the output files I have used %% and ##, this is something known as parameter expansion. 
What it does is to match what comes after ## or %% to the either the start (##) or the end (%%) of the string and remove anything that matches. In this case "./*/" = dot slash followed by any character and then slash. Removes ./data/

Bottom:

##### trim_galore.sh

```bash
#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 32
#SBATCH -J Trim_galore

files=${1} # space delimted list of input files, ${1} = input argument 1. Input zero is the script.
name=${2}

mkdir trimmed_galore/${name}
trim_galore -o trimmed_galore/${name} --nextera --fastqc --paired --retain_unpaired ${files} 

```
The above script will take pairs of sequencing read files and submit them to trim galore for clean up. 

You should now have a directory called trim galore with one folder per sample and the clean files in said folders.

### Alignment
Now we can align the reads to the reference genome.
There are multiple sequence aligners to use for ATAC-seq. The most popular one is bowtie2.
In order to align your reads to a reference genome you need a reference genome.
There is two ways to get a refrence genome:
Either you download the fasta file of your genome of interest (for human, hg38) from ensemble or refseq and index it with bowtie. This takes a while. 
Or you can download a pre-indexed version from the bowtie web page using wget.
```bash
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/GRCh38_no_alt.zip
```
Create an index directory for bowtie and unzip the index to it. It is recommended that you create this directory in your project folder since the index is quite large.
Then create a path to the index in your .bashrc file like you did for the ATAC-seq directory: `export hg38_bowtie_index=/path_to_bowtie_index` and do source .bashrc.
I would recommend to have a dedicated directory for reference genomes in the project folder.
The file to export is the file with the .fna ending.
And now align. Note that we are now going to chain multiple scripts together.

##### Align.sh

```bash
#!/bin/bash -l 

#Runs the alignment and sort the files afterwards.

#Find The sample directories
samples=$(find ./data/trimmed_galore/C* ./data/trimmed_galore/P* -prune -type d)
# This finds the forward and reverse files and gives them to bowtie. 
mkdir aligned
for i in $samples
do
    f=$(find $i/*val_1.fq.gz -type f) # Forward
    r=$(find $i/*val_2.fq.gz -type f) # Reverse
    unp=$(find $i/*unpaired*gz -type f) # Unpaired reads 
    sbatch ./bin/bowtie_run.sh "${f}" "${r}" "${unp}" ./aligned/${i##./*/} ${hg38_bowtie_index}
done


```

##### bowtie.sh

```bash
#!/bin/bash

#SBATCH -J bowtie2
#SBATCH -t 04:00:00
#SBATCH -N 1
#SBATCH -n 32

forward=$(echo ${1} | tr " " ",") # Creates comma delimited list of forward read files
reverse=$(echo ${2} | tr " " ",") # Creates comma delimited list of reverse read files
unpaired=$(echo ${3} | tr " " ",") # Creates comma delimited list of unpaired read files
ref_index=${5} # Reference genome.
out=${4}

bowtie2 -p 32 -k 10 --very-sensitive -x $ref_index -1 ${forward} -2 ${reverse} -U ${unpaired} -S ${out}.Aligned.sam --met-file ${out}.bowtie.metrics.txt

sbatch sam_to_bam.sh ${out}.Aligned.sam

```

Bowtie takes input as comma delimited list files with the forward, reverse and unpaired files seperate. Note that at the end of the script we are sending the next step of the pipeline on. 
This is very good since it means that once all the script are in order the pipeline will essentially run automatically from here.
One can write the next script into the script above but I recommend to keep dedicated scripts separate so that if things go wrong it is clear where it went wrong. 

##### sam_to_bam.sh

```bash
#!/bin/bash -l

#SBATCH -J Sam_to_bam
#SBATCH -n 1
#SBATCH -t 02:00:00
#SBATCH --mem=8000 # Megabytes of memory

sam=${1}

samtools view -bS $sam > ${sam%%.sam}.bam
samtools sort -m 6G -n ${sam%%.sam}.bam > ${bam%%.*}.sorted.bam
samtools index ${sam%%.*}.sorted.bam

rm $sam # clean up

sbatch ./bin/genrich_run.sh ${bam%%.*}.sorted.bam 
```

### Peak calling

To call peaks we will use Genrich. Genrich is a recently released peak caller recommended by the Harvard ATAC-seq guidelines. Previously they recommend MACS2.
Genrich can be found here on github: https://github.com/jsh58/Genrich. 
Genrich was produced with ATAC-seq specfically in mind while MACS2 was produces for Chip-seq so MACS2 has some quirks that just does not apply to ATAC-seq. See the Harvard ATAC-seq guidlines.
One major disvantages with Genrich though is its ridicouls ram memory usage and lack of multithreading.

Peaks will be called on all samples individually and then we will take the peaks present in at least 70% of samples within a group as the peaks defining that group of samples. 
Following this we will take the union of these four peak sets and do a limma differential expression analysis similar to what one does for RNA-seq to determine differentially 
accessible regions between the groups.

This is a very reductive approach which will hopefully produce a small high confidence peak set that we can work with for footprinting and motif matching.
One can take a less reductive approach were one calls peaks across all samples simultaneously. For this approch see version 3 of this tutorial. 
This approch was abandonded as it was deemed to produce to many false postive peaks and was very hard to evalute.

##### genrich_run.sh 
```bash
#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH --mem=24566 # 24gb Uses like 18gb

bam=$1
out=${bam%%.bam}.generich_peaks.bed

Genrich -t $bam -o $out -j -y -r -v -e chrM 
```

#### Consensus peaks

To take the consensus of the groups we will use the bedtools mulitintersect and merge functions. Multiintersect intersects the bedfiles and reports overlaps. 
Then we will use pandas in python to load the resulting bed file as a table and exclude all regions present in less than 70% of samples.
Finally we will use bedtools merge to merge the overlapping regions and take the union of the four consensus peak sets.

Note the below can be run on the login node in less than 30 seconds so no need for sbatch.
Copy the below script into the aligned folder before running it.

##### multinter.sh

```bash
#!/bin/bash -l

Patients_activated=$(find P*A_*generich_peaks.bed)
Patients_unactivated=$(find P*U_*generich_peaks.bed)
Controlls_activated=$(find C*A_*generich_peaks.bed)
Controlls_unactivated=$(find C*U_*generich_peaks.bed)

bedtools multiinter -i $Patients_activated > Patients_activated_multiintersect.bed
bedtools multiinter -i $Patients_unactivated > Patients_unactivated_multiintersect.bed
bedtools multiinter -i $Controlls_activated > Controlls_activated_multiintersect.bed
bedtools multiinter -i $Controlls_unactivated > Controlls_unactivated_multiintersect.bed

python ../atac-seq_snems/Scripts/bin/Filter_peaks.py # call the python script


for i in *consensus.bed
do 
    bedtools sort -i $i | bedtools merge -d 75 > ${i%%.bed}.merged.bed #Merges all peaks with less than 75bp (=read_length) between each other
done

#And finnaly take the union of the files
merged=$(find *merged.bed)

cat $merged | bedtools sort | bedtools merge -d 75 > Full_consensus_0.70_peaks.bed
```

###### Filter_peaks

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 16:06:01 2019

@author: x_oloru
"""

import pandas as pd
from sys import argv
from os import listdir

#Run the below over all multiintersect files. 
for i in [x for x in listdir() if "multiintersect.bed" in x]:
    df=pd.read_table(i, header=None) # Read in the bed file as a table
    #Drop unecessary columns
    df=df.iloc[:, 0:4] # Header of tabel: CHR, Start, End, Reported hits (what we want), Reported in what files, count table
    #Determine number of samples. Note this assumes that at least one region is present in all
    sample_number=max(df[3])
    #Drop areas supported by less than 70% of samples in group.
    df=df[df[3] >= 0.70 * int(sample_number)]
    #Done
    df.to_csv(i.split(".")[0] + ".consensus.bed", index=None, header=None, sep="\t")
```

### Differential peak analysis

For the limma differential peak anlysis we need to produce a count matrix of all sample for the union peak set. This can be done with many diffrent programs that counts reads in features.
We will be using Featurecounts from the rsubread package.

Run the below script from the aligned folder.

##### count_features.sh

```bash
#!/bin/bash
#SBATCH -J FeatureCounts 
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 02:00:00 # 2 h

#Activate the conda enviroment. 
#source activate NGS_env

files=$(find *Aligned.sorted.bam -type f)
# Convert the peak set into saf format. Required by featurecounts. The below is an "awk" command. It reads the bead file line by line and reorganizes the columns a bit. 
awk 'OFS="\t" {print $1"."$2+1"."$3, $1, $2+1, $3, "."}' Full_consensus_0.70_peaks.bed > Full_consensus_0.70_peaks.saf
annotation=Full_consensus_0.70_peaks.saf
output=Full_consensus_0.70_peaks.count_matrix.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files} 2> Counting_log.txt 
# 2> equals: output evrything that normaly goes to the screen to this file. The log is for checking the FRIP score of the samples and make sure evrything went fine.
# See the section extra quality controll checks. 

```

#### Lima
To be added when done.



#### Footprinting

One of the more powerful things that can be done with ATAC-seq is to look for footprints of bound transcription factors (TF). 
Footprints are detectable since when a TF binds to a region of DNA it protects it from Tn5 and this can be seen as dips in signal usually surroundeded by tall peaks.

![Inline image](./figures/Footprinting.png)

Note that the above figure is an idealized case and it usually does not look that good.

To detect footprints we will be using the HINT algorithm from the Regular Genomics Toolbox (RGT). 
HINT utelises hidden markov modeling to detect footprints. 
HINT is not the only algorithm that does this, however HINT also incoperates bias correction specfically made for ATAC-seq to find more accurate footprints.

The detection of relible footprints requires immense sequencing depths, usually in the excess of 200 million reads. 
As such we will merge all bam files within each group before footprinting with the consensus peak set.
After footprinting we will apply a cutoff messure to make sure we have robust footprints.
Then we will match the footprints to motifs and calclulate enrichment towards a random background.
Finnaly we will use rgt-hint diffrential to look at diffrential TF activity between the conditions.

##### Setup
To start the footprinting first setup the rgtdata folder. The original instruction can be found here: http://www.regulatory-genomics.org/rgt/rgt-data-folder/
The rgt data folder takes up about 18 gb so move it to your project dir and symlink it to it's original location.

```bash
cd ~/rgtdata
python setupGenomicData.py --hg38
```

Now setup the motifs and logos data. The logos are the visual representation of the motifs (PWMs). Instructions here: 

```bash
cd ~/rgtdata
python setupLogoData.py --all
```

This will generate PWMs and setup logos data for the databases HOCOMOCO, Jaspar-vertabrates and uniprobe. We will most likley use either Jaspar or HOCOMOCO.

Done.

The below analysis was partialy based on this: https://www.regulatory-genomics.org/hint/tutorial/

##### Merge bam Files

Before we can footprint we as said need to create bam files with a sufficent sequencing depth. This will be done using samools.
We also have to sort these files by postion and index them for them to work with rgt-hint.
Note that this + the footpinting will take around 2-3 days to run. 

###### merge_bams_for_footprinting_input.sh
```bash 
!#/bin/bash -l

mkdir ../../../aligned/RGT/
Activated_Patients=$(find ../../../aligned/P*A_*sorted*bam)
UnActivated_Patients=$(find ../../../aligned/P*U_*sorted*bam)
Activated_Controlls=$(find ../../../aligned/C*A_*sorted*bam)
UnActivated_Controlls=$(find ../../../aligned/C*U_*sorted*bam)

sbatch merge_sort_bam.sh "${Activated_Patients}" ../../../aligned/P_All_A.merged.sorted.bam
sbatch merge_sort_bam.sh "${UnActivated_Patients}" ../../../aligned/P_All_U.merged.sorted.bam
sbatch merge_sort_bam.sh "${Activated_Controlls}" ../../../aligned/C_All_A.merged.sorted.bam
sbatch merge_sort_bam.sh "${UnActivated_Controlls}" ../../../aligned/C_All_U.merged.sorted.bam

```

###### merge_sort_bam.sh
```bash 
!#/bin/bash -l

#SBATCH -t 48:00:00 # Depends on the amount of data. Took around 30h for me.
#SBATCH -n 32

samtools merge --threads 32 -n $1 | samtools sort -@ 32 -m 2500M > $2
samtools index $2 

sbatch rgt_hint_run.sh $2  ../../../aligned/Full_consensus_0.70_peaks.bed
```

###### rgt_hint_run.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem=15000

bam=$1
bed=$2

out=${1##../*/} # Crops input to only the file name.
rgt-hint footprinting --organism hg38 --atac-seq --paired-end --output-prefix=../../../aligned/RGT/${out%%.*}_rgt_footprints ${bam} ${bed} 
```

#####  FOS to filter out bad footprints
FOS or footprint occupancy score is a concept of caluating a score for a footprint based upon how well it agrees with the concept of shoulders and depth.
It was first applied here: doi:10.1038/nature11212

FOS = (C + 1)/L + (C + 1)/R 
C = Tag density in central component (Footprint)
L = Tag density in left shoulder
R = Tag density in right shoulder
The lower the better
Length of C = 6-40bp
Lenght of R and L = 3-10bp

FOS was in the above papper calulated as a local minima by solving an optimisation problem using varying Fp and shoulder sizes. 
However since we were not interested in redifing the postion of the footprint 
and since we are using the uncorrected ATAC-seq signal to calculate FOS we decided to set L and R to 10bp and let C = fp length.
To correct for diffrences in length between shoulders and fp the signal over fp was length corrected to 10bp (RPD).

The below solution to calculate FOS can probably be done ten times faster using the python module pysam. 
However this is not what I did, I forgot Pysam existed. See the future TH1 time series project for the pysam solution.
 
First create the shoulder and footprint files. Then count reads in these features with featurecounts and finally calculate FOS.

###### make_beds_and_count.sh
```bash
#!/bin/bash -l

python make_beds.py ../../../aligned/RGT/C_All_A_rgt_footprints.bed C_A
python make_beds.py ../../../aligned/RGT/C_All_U_rgt_footprints.bed C_U
pythonmake_beds.py ../../../aligned/RGT/P_All_A_rgt_footprints.bed P_A
python make_beds.py ../../../aligned/RGT/P_All_U_rgt_footprints.bed P_U

for i in ../../../aligned/RGT/*_C_A.bed ; do sbatch count_features.sh $i ../../../aligned/C_All_A.merged.sorted.bam ; done
for i in ../../../aligned/RGT/*_C_U.bed ; do sbatch count_features.sh $i ../../../aligned/C_All_U.merged.sorted.bam ; done
for i in ../../../aligned/RGT/*_P_A.bed ; do sbatch count_features.sh $i ../../../aligned/P_All_A.merged.sorted.bam ; done
for i in ../../../aligned/RGT/*_P_U.bed ; do sbatch count_features.sh $i ../../../aligned/P_All_U.merged.sorted.bam ; done
```


###### make_beds.py
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 09:45:58 2019

@author: x_oloru
"""

import pandas as pd
import sys

df=pd.read_csv(sys.argv[1], header=None, sep="\t")

RSh=pd.DataFrame()
RSh[0]=df[0] ; RSh[1]=df[2]+1 ; RSh[2]=df[2]+11

LSh=pd.DataFrame()
LSh[0]=df[0] ; LSh[1]=df[1]-11 ; LSh[2]=df[1]-1

Fp=pd.DataFrame()
Fp[0]=df[0] ; Fp[1]=df[1] ; Fp[2]=df[2]

RSh.to_csv("Rsh_" + sys.argv[2] + ".bed", sep="\t", header=None, index=None)
LSh.to_csv("Lsh_" + sys.argv[2] + ".bed", sep="\t", header=None, index=None)
Fp.to_csv("Fp_" + sys.argv[2] + ".bed", sep="\t", header=None, index=None)
``` 

###### count_features.sh
```bash
#!/bin/bash
#SBATCH -J FeatureCounts 
#SBATCH -N 1
#SBATCH -n 32 
#SBATCH -t 48:00:00

files=$2

#Convert bed to saf
awk 'OFS="\t" {print $1"."$2+1"."$3, $1, $2+1, $3, "."}' $1 > ${1%%.bed}.saf

annotation=${1%%.bed}.saf
output=${1%%.bed}.count_matrix.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files} 2> ${1%%.bed}.counting_log.txt
```

Now you have the read counts and necessary files to calculate FOS. The cutoff for FOS that I would recommend is 1 but this dependes on your data.
Plot the distribution and make the cutoff.

###### FOS.py
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 11:44:29 2019

@author: x_oloru
"""

import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb
import sys

Rsh = pd.read_table("../../../aligned/RGT/Rsh_" + sys.argv[1] + ".count_matrix.tsv", comment="#")
Lsh = pd.read_table("../../../aligned/RGT/Lsh_" + sys.argv[1] + ".count_matrix.tsv", comment="#")
Fp = pd.read_table("../../../aligned/RGT/Fp_" + sys.argv[1] + ".count_matrix.tsv", comment="#")

# Two exlusion criteria 
#   1. Rsh and Lsh must be greater than Fp
#   2. FOS should be below 1 (Rsh = Fp + 1, Lsh =Fp + 1 would lead to FOS=2)

# 1 Rsh and Lsh > Fp
FOS=pd.DataFrame()
FOS["Chr"]=Fp.iloc[:,1] ; FOS["Start"] = Fp.iloc[:,2] ; FOS["End"] = Fp.iloc[:,3] 
FOS["Lsh"] = Lsh.iloc[:,6] 
#Convert Fp reads values to RPD (Reads Per Deka base pairs)
#I.E Length correct to 10 bp to account for diffrences in Fp length
FOS["Fp"] = Fp.iloc[:,6] * 10/Fp.iloc[:,5]
FOS["Rsh"] =  Rsh.iloc[:,6]
FOS["FOS"] = (FOS["Fp"] + 1)/FOS["Rsh"]  + (FOS["Fp"]  + 1)/FOS["Rsh"] 

# Some plots For documentation
# Depth over Fp, Rsh and Lsh
plt.ylim(0, 0.015) ; sb.distplot(FOS[FOS["Fp"] < 2000]["Fp"], bins = 1000 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Fp_count_distribution.png") ; plt.close()
plt.ylim(0, 0.015) ; sb.distplot(FOS[FOS["Rsh"] < 2000]["Rsh"], bins = 1000 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Rsh_count_distribution.png") ; plt.close()
plt.ylim(0, 0.015) ; sb.distplot(FOS[FOS["Lsh"] < 2000]["Lsh"], bins = 1000 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Lsh_count_distribution.png") ; plt.close()

plt.ylim(0, 0.05) ; sb.distplot(FOS[FOS["Fp"] < 100]["Fp"], bins = 100 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Fp_count_distribution_less_than_100.png") ; plt.close()
plt.ylim(0, 0.05) ; sb.distplot(FOS[FOS["Rsh"] < 100]["Rsh"], bins = 100 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Rsh_count_distribution_less_than_100.png") ; plt.close()
plt.ylim(0, 0.05) ; sb.distplot(FOS[FOS["Lsh"] < 100]["Lsh"], bins = 100 ) ; plt.savefig("./plots/" + sys.argv[1] + "_Lsh_count_distribution_less_than_100.png") ; plt.close()

# Now exlude based on 1
FOS=FOS[FOS["Rsh"] > FOS["Fp"]]
FOS=FOS[FOS["Lsh"] > FOS["Fp"]]
# And now based on FOS and shoulder depth. Depth cutoff was set to 10 reads.
# FOS = (C+1)/R + (C+1)/L

# Dist of FOS
sb.distplot(FOS["FOS"], bins = 20) ; plt.savefig("./plots/" + sys.argv[1] + "_FOS_distribution.png") ; plt.close()

# A now exlude
FOS = FOS[FOS["Lsh"] > 10]
FOS = FOS[FOS["Rsh"] > 10]
FOS = FOS[FOS["FOS"] < 1]

Footprints = pd.read_table(sys.argv[2] + ".bed", header=None)
Footprints = Footprints[Footprints.index.isin(FOS.index)]

Footprints.to_csv(sys.argv[2] + "filtered_FOS.bed", sep="\t", index=None, header=None)
```

Starting the above:
```bash
mkdir ../../../aligned/RGT/plots
python FOS.py P_A ../../../aligned/RGT/P_All_A_rgt_footprints
python FOS.py P_U ../../../aligned/RGT/P_All_U_rgt_footprints
python FOS.py C_A ../../../aligned/RGT/C_All_A_rgt_footprints
python FOS.py C_U ../../../aligned/RGT/C_All_U_rgt_footprints
```

With the above methods roughly two thirds of all footprints were excluded reducing the number of 
discovered footprints from about 185 000 to 65 000 per group. 

###### Motif matching and enrichment
Next we will match all of our footprints to motifs.
The default database is jaspar veterbrates. 
If you want to use another database like hocomoco that has more TFs you can't do that through the 
command line and instead have to set it in the rgt user config file.

First open your rgtdata/data.config.user file and copy in the below lines to the end of the file:

```
[MotifData]
repositories: hocomoco
```
This will make it so that all future motif matches are with hocomoco. 

And now sbatch the matching script.

###### rgt_motifanalysis_matching.sh
```bash
#SBATCH -J rgt
#SBATCH -n 1
#SBATCH -t 02:00:00 

files=$(find ../../../aligned/RGT/*footprintsfiltered_FOS.bed)
rgt-motifanalysis matching --organism hg38 --rand-proportion 10 --input-files $files

bash rgt_motifanalysis_enrichment_input.sh
```
Next we check if there are any specific motifs enriched within the groups.

###### rgt_motifanalysis_enrichment_input.sh
```bash
#!/bin/bash -l

#Note this folder is most practical to have in the script folder. 
#For organizational reasons however move it to the rgt folder and symlink it back into the scripts folder.
background=$(find ./match/random_regions.bed)

for i in ../../../aligned/RGT/*rgt_footprintsfiltered_FOS.bed
do
    sbatch rgt_motifanalysis_enrichment_run.sh $background $i
done
```

###### rgt_motifanalysis_enrichment_run.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-enrichment
#SBATCH -n 1
#SBATCH -t 02:00:00

background=$1
fp=$2
rgt-motifanalysis enrichment --organism hg38 $background $fp
```

###### Diffrential TF activity test.

A cool test that can be done with RGT-hint is to check for diffrenses in "TF activity" (it is more like how much does this TF influence one condtion over the other). 
This is done with rgt-hint diffrential


###### rgt-hint diffrential_input.sh
```bash
#!/bin/bash -l

#Controll agianst patients
sbatch rgt_hint_differential_run.sh ../../../aligned/P_All_A.merged.sorted.bam ../../../aligned/C_All_A.merged.sorted.bam ./match/P_All_A_rgt_footprintsfiltered_FOS_mpbs.bed ./match/C_All_A_rgt_footprintsfiltered_FOS_mpbs.bed MS_A Healthy_A 
sbatch rgt_hint_differential_run.sh ../../../aligned/P_All_U.merged.sorted.bam ../../../aligned/C_All_U.merged.sorted.bam  ./match/P_All_U_rgt_footprintsfiltered_FOS_mpbs.bed ./match/C_All_U_rgt_footprintsfiltered_FOS_mpbs.bed MS_U Healthy_U

#Activated vs unactivated
sbatch rgt_hint_differential_run.sh ../../../aligned/P_All_U.merged.sorted.bam ../../../aligned/P_All_A.merged.sorted.bam ./match/P_All_U_rgt_footprintsfiltered_FOS_mpbs.bed ./match/P_All_A_rgt_footprintsfiltered_FOS_mpbs.bed MS_U MS_A
sbatch rgt_hint_differential_run.sh ../../../aligned/C_All_U.merged.sorted.bam ../../../aligned/C_All_A.merged.sorted.bam ./match/C_All_U_rgt_footprintsfiltered_FOS_mpbs.bed ./match/C_All_A_rgt_footprintsfiltered_FOS_mpbs.bed Healthy_U Healthy_A
```

###### rgt_hint_differential_run.sh 
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 24 
#SBATCH -t 08:00:00
#SBATCH --mem=92000 # memory usage in mb

reads1=$1
reads2=$2
mpbs1=$3
mpbs2=$4

con1=$5
con2=$6

rgt-hint differential --nc 24 --bc --organism hg38 --mpbs-file1 $mpbs1 --mpbs-file2 $mpbs2 --reads-file1 $reads1 --reads-file2 $reads2 --condition1 $con1 --condition2 $con2 --output-location ../../../aligned/RGT/${con1}_vs_${con2}_differential_fp
```

Now what?


### Extra quality control checks

For ATAC-seq there is a number of specific quality controll checks that you may perform to check the quality of your data. These are:
```
	1. Fragment size distribution
	2. TSS score
	3. Frequency of reads in peaks (FRIP)
```
For a through explanation check the encode project webpage on sequencing data quality.

#### Fragment size distribution

First we will plot the fragment size distribution. This is a measure of the library complexity of your original samples. 
What you want to see here is that you have clear nuclesome band pattern in the logaritimic plot. 
And a sharp peak at < 100bp for sort inserts from open regions of chromatin.

Make a directory called fragement_size_plotting in the aligned folder and cd into it.
Copy the below scripts into it. Note this will be automatic eventually.

Source for the below: https://dbrg77.wordpress.com/2017/02/10/atac-seq-insert-size-plotting/

#### record_fragement_sizes.sh
```bash
#!/bin/bash -l

#SBATCH -n 32
#SBATCH -t 00:30:00 # Note takes like 5 minutes

mkdir plots

for i in ../*sorted.rmdup.filtered.bam
do
	samtools view $i | awk '$9>0' | cut -f 9 | sort | uniq -c | sort -b -k2,2n | sed -e 's/^[ \t]*//' > ${i%%bam}fragment_length_count.txt &
done
```

This is what the samtools command does:
```bash
samtools view $i | #stream reads
awk '$9>0' | #output reads with positive fragment length, i.e. left-most reads
cut -f 9 | #get the fragment length
sort | uniq -c | #count the occurrence of each fragment length
sort -b -k2,2n | #sort based on fragment length
sed -e 's/^[ \t]*//' #remove leading blank of the standard output
```
And now plot the resulting tables with pyplot in python.

#### plot_fragement_size.py

```python
#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot as plt
from os import listdir

for i in [x for x in listdir() if "fragment_length_count" in x]: # Get all Fragment length count files
    ma = np.loadtxt(i, dtype="int") # Load in as 2d numpy array
    sample_name=i.split("_")[0] # Get the sample name
    plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0]) ; plt.savefig("./plots/" + sample_name + "_linear_fragment_size.png") ; plt.close() # plot Linear graph
    plt.semilogy() ; plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0]) ; plt.savefig("./plots/" + sample_name + "_log_fragment_size.png") ; plt.close() # plot Log graph
```
Note the above pyplot commands would normaly be written line by line but semicolon can be used as line breaks instead to compact the code.
This commes at the cost of readabilty though so this is a personal preference question. Semicolon can be used in the same way in bash.

#### Fragment size example plots

Good Linear plot

![Inline image](../Quality_Controll/Fragment_size_plots/C10A_linear_fragment_size.png)

Log plot for same sample

![Inline image](../Quality_Controll/Fragment_size_plots/C10A_log_fragment_size.png)

Bad Linear plot. Observe the lack of variety in fragment sizes.

![Inline image](../Quality_Controll/Fragment_size_plots/P6A_linear_fragment_size.png)

Log plot for same sample

![Inline image](../Quality_Controll/Fragment_size_plots/P6A_log_fragment_size.png)

#### TSS score

Tss score is a measurement of signal to noise ratio used in ATAC-seq. 
It is calculated by taking the average of the ratio between the read depth at the transcription start site (Tss) and the read depth 3000bp away from it for every gene in the genome.
3000bp up or downstream of TSS is usually used as the boundaries of the gene's promotor. 

There is a python package available that does the above for you, install it.
`pip install tssenrich`

##### tss_enrichment_score_input.sh

```bash
#!/bin/bash -l

mkdir tss_scores

for i in *sorted.rmdup.bam
do
    sbatch tss_enrichment_score.sh $i
done
```

###### tss_enrichment_score.sh

```bash
#!/bin/bash -l

#SBATCH -t 01:00:00
#SBATCH -n 1
#SBATCH --mem=6000

tssenrich --genome hg38 --memory 3 --processes 1 --log tss_scores/${1%%.*}.log.txt $1 > ./tss_scores/${1%%.*}.tss_score.txt
```
 
Now cd into the tss_scores directory and summarize the results

##### summarize_scores.sh

```bash
#!/bin/bash -l

touch All_scores.txt
for i in *tss_score.txt 
do
    name=${i%%.*} # get sample name
    value=$(cat $i) # Get value
    echo $name $value >> All_scores.txt # Append to file
done
```

The above will summarize all of the scores into a tab delimited table. 
This should in theory be possible to do with tssenrich in one command if the instructions are to be believed but I could not find a way to record what score belonged to what file.

We will plot this later together with the FRIP score.

#### Plot tss enrichment score, FRIP and number of peaks.
Now we will use the matplotlib addition seaborn to plot tss score, FRIP and the number of peaks across all samples to get an overview of signal to noise ratio in our samples.
Note the FRIP score and the number of detected peaks is correlated to the tss score so these three plots essentily show the same thing. 

First get the FRIP score into a nice table format from the counting log. `bash get_frip.sh > frip_score.csv`

##### get_frip.sh

```bash
#!/bin/bash -l

frip=$(grep -R "Successfully assigned fragments" Counting_log.txt) # Grep the line with the precentage
frip=$(cut -d "(" -f 2 <<< "$frip") # Get the interesting half
frip=$(cut -d ")" -f 1 <<< "$frip") # get the precentage
# Make into list
arrfrip=($frip)

# Now get the sample names
samples=$(grep -R "Process BAM file" Counting_log.txt) # Get the line
samples=$(cut -d "." -f 1 <<< "$samples") # Get the interesting half
samples=$(cut -d " " -f 5 <<< "$samples") # get the sample name
#Make into list
arrsamples=($samples)

#Print to standard out as array
for index in ${!arrsamples[*]} 
do
    echo ${arrsamples[$index]}","${arrfrip[$index]}
done
```

And make a table of the number of peaks per file.

```bash
#!/bin/bash -l 

touch number_of_peaks.tsv
for i in *generich_peaks.bed
do
	n=$(wc -l $i)
	sample=${i%%_*bed}
	echo $sample $n >> number_of_peaks.tsv
done
```

And now plot. I would recommend to start Ipython and run the below by pasting it into the terminal.

##### make_bar_plot.py
```python 
#!/usr/bin/env python

import pandas as pd
import seaborn as sb
from matplotlib import pyplot as plt

#FRIP
frip=pd.read_csv("frip_score.csv", sep=",", header=None)
frip[1] = frip[1].str.strip("%") 
frip[1] = pd.to_numeric(frip[1]) 

plt.figure(figsize=(18.5, 10))
frip_bars=sb.barplot(x=frip[0], y=frip[1]) 
plt.ylabel("FRIP(%)")

for item in frip_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("frip_bars.svg") 

#Number of peaks
peaks=pd.read_csv("All_scores.txt", sep=" ", header=None)

plt.figure(figsize=(18.5, 10))
peaks_bars=sb.barplot(x=peaks[0], y=peaks[1]) 
plt.ylabel("Number of Peaks")

for item in peaks_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("peaks_bars.svg") 

cd tss_scores
#tss
tss=pd.read_csv("All_scores.txt", sep=" ", header=None)

plt.figure(figsize=(18.5, 10))
tss_bars=sb.barplot(x=tss[0], y=tss[1]) 
plt.ylabel("TSS Score")

for item in tss_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("Tss_bars.svg") 
```

#### Tss plot

![Inline image](../Quality_Controll/Tss_bars.png)


### Results visualization

#### Intervene for peaks

A good way to visualize what peaks are shared between different files is through "Intervene upset". 
This produces a bar graph of the intersection between peaks.

Intervene can be installed into conda however on Sigma this causes a problem with the lib dependencies. 
The recommended way to install Intervene on sigma is to deactivate the current conda env and install it to your user python with pip. `pip install --user intervene`.
To use intervene you need to activate your conda env again since intervene requires bedtools to work and bedtools does not work outside conda on the cluster. This also due to lib dependency problems.
Note if you accidently update your user pip on the cluster it will stop working and you will have to write `python -m pip install --user <package>` to install in the future.

![Inline image](../Quality_Controll/Top_Peaks_comp_cons_groups_upset.pdf)

The above is only practical for up to eight files though since it gets very messy when the number of intersections goes up.
Therefore to visualize the intersection between individual samples we will instead use the pairwise function. This produces a heatmap representation of the intersections.

##### upset_plot.sh 
```bash
#!/bin/bash -l

#SBATCH -t 08:00:00 # Take around 2h
#SBATCH -n 1 

Group_peaks=$(find *consensus.merged.bed)
intervene upset --names=C_A,C_U,P_A,P_U --project=Top_Peaks_groups -i ${Group_peaks}
intervene upset --names=C_A,C_U,P_A,P_U,Consensus --project=Top_Peaks_groups_comp -i ${Group_peaks} Full_consensus_peaks.bed 

# Intersect within the groups
Patients_activated=($(find P*A_*peaks.bed))
Patients_unactivated=($(find P*U_*peaks.bed))
Controlls_activated=($(find C*A_*peaks.bed))
Controlls_unactivated=($(find C*U_*peaks.bed))

# Make the header for each group
PA_names=() ; PU_names=() ; CA_names=() ; CU_names=() # define the lists
for index in ${!Patients_activated[*]} ; do PA_names+=(${Patients_activated[$index]%%.*}) ; done # Append names to list
for index in ${!Patients_unactivated[*]} ; do PU_names+=(${Patients_unactivated[$index]%%.*}) ; done # Append names to list
for index in ${!Controlls_activated[*]} ; do CA_names+=(${Controlls_activated[$index]%%.*}) ; done # Append names to list
for index in ${!Controlls_unactivated[*]} ; do CU_names+=(${Controlls_unactivated[$index]%%.*}) ; done # Append names to list
# ${!array[*]} gets the index of the array
#Convert to coma delimited string
IFS=, eval 'PA="${PA_names[*]}"'
IFS=, eval 'PU="${PU_names[*]}"'
IFS=, eval 'CA="${CA_names[*]}"'
IFS=, eval 'CU="${CU_names[*]}"'

# IFS is the global separator. This is normally set to whitespace but can be changed to any other character. 
# In the above case IFS is only locally modified for this line. If you globally modify IFS remember to change it back afterwards.
 
intervene pairwise --names=${PA} --project=Patients_activated -i ${Patients_activated[@]}
intervene pairwise --names=${PU} --project=Patients_unactivated -i ${Patients_unactivated[@]} 
intervene pairwise --names=${CA} --project=Controlls_activated -i ${Controlls_activated[@]} 
intervene pairwise --names=${CU} --project=Controlls_unactivated -i ${Controlls_unactivated[@]}

#For all. Note this will be almost illegible.
intervene pairwise --figtype svg --figsize 40 25 --names=${PA},${PU},${CA},${CU} --project=All_sampels 
    -i ${Patients_activated[@]} ${Patients_unactivated[@]} ${Controlls_activated[@]} ${Controlls_unactivated[@]}
```

From These heatmaps one can tell if a subset of samples within a group produces a diffrent set of peaks. 
This can be used as a resoning to either exlude these samples or take a closer look at them.





