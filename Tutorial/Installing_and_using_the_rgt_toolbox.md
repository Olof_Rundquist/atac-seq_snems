## The regular genomics Toolbox

The RGT Toolbox is a set of tools for footprinting and motif analysis. See webpage for additional info: http://www.regulatory-genomics.org/rgt/basic-introduction/

I will update the Tutorial with rgt and Genrich later.

### Instalation

If you have the ngs env from the ATAC-seq tutorial first try installing rgt into that one as it should be python 2.7.
If not create a new conda env with python 2.7. Rgt does not work with pyton 3.6. 


```bash
source activate NGS_env
conda install pip
pip install cython numpy scipy
pip install RGT
```

The above worked for me. Alternatively you can install rgt as a python 2.7 package into your user python packages but I do not know how to do that.

Then setup the rgtdata folder. The original instruction can be found here: http://www.regulatory-genomics.org/rgt/rgt-data-folder/
The rgt data folder takes up about 18 gb so move it to your project dir and symlink it to it's original location.

```bash
cd ~/rgtdata
python setupGenomicData.py --hg38
```

Now setup the motifs and logos data. The logos are the visual representation of the motifs (PWMs). Instructions here: 

```bash
cd ~/rgtdata
python setupLogoData.py --all
```

This will generate PWMs and setup logos data for the databases HOCOMOCO, Jaspar-vertabrates and uniprobe. We will most likley use either Jaspar or HOCOMOCO.

Done.

### Usage

The below test was based on this: https://www.regulatory-genomics.org/hint/tutorial/

#### Footprinting with rgt-hint

Footprinting with rgt-hint is very straight forward and very fast. Footprinting on 20 000 - 30 000 peaks can successfully be run on one core in less than 5 minutes per sample.
Note that for higher number of peaks additional cores and time will be needed due to ram usage. Ex 600 000 peaks uses 10 gb of ram and takes about 3h per sample.
Footprinting with rgt-hint is not multi-threaded.
Also note that rgt hint produces an extreme number of footprints since it does not make a cutoff.

To run rgt for all samples on a set of peaks do the following: 
What set will have to be discussed.

##### rgt_hint_input.sh 
```bash
#!/bin/bash -l

# Use the deduplicated and filtered bam files. 
# Ideally we should use files filtered and deduplicated in the same way as Genrich does but there does not seem to be an option for Genrich to output the deduplicated bam file.

bed=Activation.bed # I tested with the bed file generated from Genrich by comparing unactivated vs activated.

for i in *.rmdup.filtered.bam
do
sbatch rgt_hint_run.sh $i $bed
done 
```

##### rgt_hint_run
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 00:30:00

bam=$1
bed=$2

out=${1##../*/} # Crops input to only the file name.
rgt-hint footprinting --organism hg38 --atac-seq --paired-end --output-prefix=${out%%.*}_activation_rgt_fp ${bam} ${bed} 

```

#### Match to motifs

Next we will match all of our footprints to motifs. This is done serially for all samples and takes about 20 minutes.
The default database is jaspar veterbrates. If you want to use hocomoco that has more TFs you can't do that through the command line and instead have to set it in the rgt user config file.

First open your rgtdata/data.config.user file and copy in the below lines to the end of the file:

[MotifData]

repositories: hocomoco

This will make it so that all future motif matches are with hocomoco. 

So now matching:

rgt_motif_match.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 02:00:00

rgt-motifanalysis matching --organism hg38 --rand-proportion 10 --input-files *_activation_rgt_fp.bed

```

The --rand-proportion 10 argument makes a  randomised background to compare to for enrichment anlaysis. We won't be using it as we will be using rgt-hint differential instead to compare against the controls.

This will produce one mpbs (motif predicted binding site) file for each sample in the ./match directory

#### Differential analysis

This is what I tested out.
```bash 
rgt-hint differential --organism hg38 --mpbs-file1 ./match_all/C7A_Ktrl-74_activation_rgt_fp_mpbs.bed --mpbs-file2 ./match_all/C7U_Ktrl-74_activation_rgt_fp_mpbs.bed --reads-file1 ../../../C7A_Ktrl-74.sorted.rmdup.filtered.bam --reads-file2 ../../../C7U_Ktrl-74.sorted.rmdup.filtered.bam --condition1 Act --condition2 UAct --nc 3
```

To do the same for all samples:

##### rgt_hint_differential_input.sh

```bash
#!/bin/bash -l

A_reads=($(find ../../*A_*.sorted.rmdup.filtered.bam))
U_reads=($(find ../../*U_*.sorted.rmdup.filtered.bam))

A_mpbs=($(find ./match/*A_*activation*mpbs.bed))
U_mpbs=($(find ./match/*U_*activation*mpbs.bed))

for index in ${!A_reads[*]}
do
	sbatch rgt_hint_differential_run.sh ${U_reads[$index]} ${A_reads[$index]} ${U_mpbs[$index]} ${A_mpbs[$index]}
done
```

##### rgt_hint_differential_run.sh
```bash
#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 8 # Rgt-hint differential can not efficiently use more than 8 threads. I have tested this.
#SBATCH -t 01:00:00

reads1=$1
reads2=$2
mpbs1=$3
mpbs2=$4

rgt-hint differential --organism hg38 --mpbs-file1 $mpbs1 --mpbs-file2 $mpbs2 --reads-file1 $reads1 --reads-file2 $reads2 --condition1 Act --condition2 UAct --nc 8 --output-location ./${mpbs1##./*/}_differential 

```

This will produce a differential activation profile for evry sample. These profiles could then be compared to determine what Tfs define each sample group and if there is a difference in patients and controlls.
