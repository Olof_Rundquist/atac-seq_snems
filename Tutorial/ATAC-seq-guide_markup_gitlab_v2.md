## ATAC-seq analysis guide on sigma
Hello! Welcome to my tutorial for ATAC-seq anlysis on the gamma/sigma cluster. This tutorial will walk you through at a very basic level how to setup ATAC-seq analysis on sigma. 
The tutorial is written to be understandable to people without prior knowledge of programing or unix based operating systems.
Therefore a crash course in both is given as well. The principles outlined in this tutorial also applies to other nges analysis pipelines. Good luck.

### Getting started 
Start by acquiring access to the sigma cluster by following the instructions on the snic webpage.
Connect to the cluster through ssh. On a Linux system this should be directly available in the terminal window. 
On windows I recommend you to download mobaxterm and log in through its terminal. Download the portable version since it bypasses the need for Administrator rights.
Alternatively use thinlinc.

Once logged in load the Anaconda module. Then create a conda virtual environment in your local folder. 
```bash
module load Python/3.6.3-anaconda-5.0.1-nsc1
conda create -n enviroment_name
source activate enviroment_name

#Set up channels to install packages
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```
Bioconda is conda repository for biological analysis tools. It contains many useful packages for HTseq analysis. Simply google bioconda to check their available packages.

Through out this tutorial we will be using many different types of software. 
Too install all of them at once use the below command.
It will take awhile install everything.

```bash
conda install -y fastqc multiqc trim-galore bowtie2 samtools picard bedtools macs2 pysam meme subread
```

To make it so that your anaconda module is always loaded open .bashrc in your home directory and add the line `module load Python/3.6.3-anaconda-5.0.1-nsc1` to it.
While your at it for easy access to your ATAC-seq data also add it's location as an environment varibel: `export ATAC-seq_data_dir=<path_to_ATAC-seq_directory>`. 
You can now access your ATAC-seq data directory by simply typing `cd $ATAC-seq_data_dir`

### Setup your data directory.
It pays to have some organization.

```bash
mkdir ./ATAC-seq_data
mkdir ./ATAC-seq_data/data
```

move all of your raw data folders into the data folder.
Put all scripts in the directory above (ATAC-seq_data)
The assumed working directory for the turotial is the ATAC-seq_data/ folder.
Name this folder based on the ATAC-seq project and create similar separate folder for different sequencing projects.
When you are done with a project always keep the scripts around so that you can go back to them to check what you did. 
Also keep a README file for each project directory that outlines what it was and what was done with it.
If necessary also keep an installation guide for all of the programs.
This way you will know what script was used for each project.

### Pre-Alignment processing 

The first step of high throughput sequencing (HTseq) analysis is alignment of sequencing reads to the reference genome. 
However before we can do this we must ensure that the sequencing data is of good quality. To check sequencing quality Fastqc is usually used.
Fastqc takes a while to run so do not run it on the login node. Cluster computers like sigma uses a job system called SLURM to handle user job submissions. 
The key to efficient cluster usage is parallelization of inputs. There are several ways to achieve this and some of them are outlined on the SNIC website. The way I will outline is by using for loops.
For Fastqc the for loop won't be necessary since the parallelization is built into the program itself. Fastqc will check the number of reads, read quality, adapter content etc of your files and output a html report for each file. 
For each html report it will also output a zip file.
This report is quite useful but if you have many files it is very cumbersome to go through all of the reports. We will therefore compile all of the reports using multiqc. 
Open a document in a text editor (I recommend emacs or spyder) called fastqc.sh.

fastqc.sh
```bash
#!/bin/bash -l

#Below is the parameter for sbatch. To see the avilabel parmameter write sbatch --help on the command line.
#SBATCH -J fastqc # Name of the job
#SBATCH -t 02:00:00 # The wall time. If exceeded the script will be killed.
#SBATCH -n 32 # The number of cores to use. There is 32 cores per node.

source activate enviroment_name

#make the output directory for the reports
mkdir $2

#$1 is ./data/*/*fastq.gz 
#-t denotes the maximum number of files to process in parallel. Each uses about 250 mb of ram and we have 96 gb per node so 384 per node is max
fastqc -t 384 -o $2 $1  
# If you have less than 384 samples adjust the -t and SBATCH -n accordingly. If you try to open more than the available ram of threads fastqc will fail to start. 

#cd into output directory and run multiqc.
cd $2

multiqc *zip
```

So what is this? This is a bash script. The first line of a any given script is the shebang line. 
The shebang line tells the computer what language to use when reading the file and will vary with the programing language.
To submit the above script to the job queue write `sbatch fastqc.sh ./data/*/*fq.gz ./fastqc_reports`. 
This will run the script on all files ending with fq.gz in your ATAC-seq data directory and output the reports in a folder called fastqc_reports.

Change directory into the fastqc_reports directory.
Write `xdg-open multiqc_report.html`to open the report.
The important part of this report is the number of reads, the quality score and the adapter content.
If the adapter content is none and the average quality is close to 40 you can skip the trim-galore section. You should still read it as it introduces how to parallelize job scripts. 
The report will also give you information on duplicated sequences and repetitive sequences (kmers). 
ATAC-seq data will fail the quality check for both of these since we are sequencing very specific regions of the genome. 
This is in our case fine but if your data is for example RNA-seq this should not occur.

#### Adapter trimming and removal of low quality reads 
If you have sequencing adapters in the data you need to remove them before aligning your reads to the genome. Otherwise your alignment rate will be low.
There are many different programs for this but we will be using trim galore
The reason why we are using trim galore is because it maintains the structure of the input file. This is vital if you have pair-end sequencing data as otherwise your reads will no longer be paired.
Trim galore also sort out unpaired reads and bad quality reads. 
To speed up this step we are going to submit each sample as a separate job. This we do by creating a top level scripts that gives input to a low level script that runs the program.


Top:

trim_galore_submit.sh
```bash
#!/bin/bash -l

#How to write the find command depends on the look of your files.
#In order to save an output from a command to a variable in bash you use: `varibel=$(cmd)` as seen below.

#Find all of the sample directories
samples=$(find ./data/C* ./data/P* -prune -type d)

#give the files for each sample to trim galore. 
#Trim galore expects input on the form of forrward reverses forward reverse ...
#Simply find all files for each sample and give them as a group.
mkdir trimmed_galore
for i in $samples
do
    files=$(find $i/*/*.fq.gz -type f) 
    sbatch ./bin/trim_galore.sh "${files}" ${i##./*/} # ${i##./*/} = sample name 
done
# "${files}" sends the whole space delimted list as one argument (a string).
# take note that Bash processes things like {} and $ before the " " so "${files}" is read as the string of the varibel files. This is quite different from how f.ex python works. 
# Note if you omit the " " then the varible will be given as a list of argumnets rather than just one argument.

```
For the output files I have used %% and ##, this is something known as parameter expansion. 
What it does is to match what comes after ## or %% to the either the start (##) or the end (%%) of the string and remove anything that matches. In this case "./*/" = dot slash followed by any character and then slash. Removes ./data/

Bottom:

trim_galore.sh

```bash
#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 32
#SBATCH -J Trim_galore

files=${1} # space delimted list of input files, ${1} = input argument 1. Input zero is the script.
name=${2}

mkdir trimmed_galore/${name}
trim_galore -o trimmed_galore/${name} --nextera --fastqc --paired --retain_unpaired ${files} 

```
The above script will take pairs of sequencing read files and submit them to trim galore for clean up. 

You should now have a directory called trim galore with one folder per sample and the clean files in said folders.


#### Alignment
Now we can align the reads to the reference genome.
There are multiple sequence aligners to use for ATAC-seq. The most popular one is bowtie2.
In order to align your reads to a reference genome you need a reference genome.
There is two ways to get a refrence genome:
Either you download the fasta file of your genome of interest (for human, hg38) from ensemble or refseq and index it with bowtie. This takes a while. 
Or you can download a pre-indexed version from the bowtie web page using wget.
```bash
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/GRCh38_no_alt.zip
```
Create an index directory for bowtie and unzip the index to it. It is recommended that you create this directory in your project folder since the index is quite large.
Then create a path to the index in your .bashrc file like you did for the ATAC-seq directory: `export hg38_bowtie_index=/path_to_bowtie_index` and do source .bashrc.
I would recommend to have a dedicated directory for reference genomes in the project folder.
The file to export is the file with the .fna ending.
And now align. Note that we are now going to chain multiple scripts together.

Align.sh

```bash
#!/bin/bash -l 

#Runs the alignment and sort the files afterwards.

#Find The sample directories
samples=$(find ./data/trimmed_galore/C* ./data/trimmed_galore/P* -prune -type d)
# This finds the forward and reverse files and gives them to bowtie. 
mkdir aligned
for i in $samples
do
    f=$(find $i/*val_1.fq.gz -type f) # Forward
    r=$(find $i/*val_2.fq.gz -type f) # Reverse
    unp=$(find $i/*unpaired*gz -type f) # Unpaired reads 
    sbatch ./bin/bowtie_run.sh "${f}" "${r}" "${unp}" ./aligned/${i##./*/} ${hg38_bowtie_index}
done


```

bowtie.sh

```bash
#!/bin/bash

#SBATCH -J bowtie2
#SBATCH -t 04:00:00
#SBATCH -N 1
#SBATCH -n 32

forward=$(echo ${1} | tr " " ",") # Creates comma delimted list of forward read files
reverse=$(echo ${2} | tr " " ",") # Creates comma delimted list of reverse read files
unpaired=$(echo ${3} | tr " " ",") # Creates comma delimted list of unpaired read files
ref_index=${5} # Reference genome.
out=${4}

bowtie2 -p 32 --very-sensitive -x $ref_index -1 ${forward} -2 ${reverse} -U ${unpaired} -S ${out}.Aligned.sam --met-file ${out}.bowtie.metrics.txt

sbatch remove_duplicates_picard.sh ${out}.Aligned.sam

```

Bowtie takes input as comma deleimted list files with the forward, reverse and unpaired files seperate. Note that at the end of the script we are sending the next step of the pipeline on. 
This is very good since it means that once all the script are in order the pipeline will essentially run automatically from here.
One can write the next script into the script above but I recommend to keep dedicated scripts separate so that if things go wrong it is clear where it went wrong. 
The next scripts removes duplicated reads from the ATAC-seq alignments and compressed the samfile to a bamfile. Duplicates are removed since ATAC-seq has problems with PCR-bias leading to a high rate of duplicated sequences. 
By removing them many false positive peaks are removed.

remove_duplicates_picard.sh

```bash
#!/bin/bash -l

#SBATCH -J Picard-rmdup
#SBATCH -n 2
#SBATCH -t 02:00:00

#For unknown reasons samtools sometimes truncates the file when removing duplicates.
#Therfore we are using picard instead.
bam=${1}
picard MarkDuplicates I=${bam} M=${bam%%.*}.duplicate.metrics.txt O=${bam%%.*}.sorted.rmdup.bam REMOVE_DUPLICATES=true

samtools index ${bam%%.*}.sorted.rmdup.bam

sbatch macs2.sh ${bam%%.*}.sorted.rmdup.bam

```

And now call peaks with MACS2.

macs2.sh

```bash
#!/bin/bash
#SBATCH -J MACS2 # A single job name for the array
#SBATCH -n 32 # Processes
#SBATCH -t 00:45:00 # 1 h

bam=${1}

mkdir ${bam%%.*}_macs_peaks # macs will fail if the outdir does not excist.
macs2 callpeak --format BAMPE -t ${bam} --outdir ${bam%%.*}_macs_peaks -n ${bam%%.*} --nomodel

```

Now I would recommend to delete the .sam files since they take up a lot of space and can be recreated with samtools if necessary.
Now you should have one peak file per sample. What you do next is to footprint and look for binding motifs. However how you do so depends a bit on your experimental question. In our case we had a case control study comparing the effects of a treatment in healthy people vs patients.

Therefore to pick out peaks interesting to us we used limma to select out differentially "expressed" peaks.

To do a limma analysis you need a count matrix of raw read counts in all peaks in all samples. 
To create a count matrix from the macs2 output first merge all peaks files with bedtools to get all regions with ATAC-seq signal in the data. 

bedtools_merge.sh
```bash
#!/bin/bash -l
#SBATCH -J bed-merge
#SBATCH -n 4
#SBATCH -t 00:30:00

files=$(find aligned/*/*narrowPeak)

cat $files | bedtools sort | bedtools merge > merged.peaks.bed
```

Then to count the reads in all peaks in all samples use featurecounts from the subread package.

count_features.sh
```bash
#!/bin/bash
#SBATCH -J FeatureCounts # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 02:00:00 # 2 h

#Feature counts requires the input in saf format so convert to it from bed. The below awk command does the trick. 
awk 'OFS="\t" {print $1"."$2+1"."$3, $1, $2+1, $3, "."}' merged.peaks.bed > peaks.saf
files=$(find aligned/*.bam -type f)
annotation=peaks.saf
output=count_matrix.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files}

```

Now download the count matrix to your local computer for the limma analysis and use the provided R script.

After the limma you will have a number of peak files with candidate peaks. 

Now things becomes a bit iffy and I do not know how things should be done excatly.

Below is some good things to do:

####Motif Matching
To match the sequences of the peaks in any bed file (we will use merge.peaks.bed as an exampel)

First get the sequence of the peaks using the below python script.
Deactivate the env before running the below script as some of the programs installed in your env depends on python 2.7 and your env therefore uses that as default. The below script is writen for python 3.6.
Then install the necessary python packages and run it.

```bash
pip install --user pysam
pip install --user pandas
```

fetch_genomic_region.py
```python
#!/usr/bin/env python

import pysam
import pandas as pd
import sys

genome=sys.argv[1]
bed=sys.argv[2]

genome=pysam.FastaFile(sys.argv[1])
bed=pd.read_table(sys.argv[2], header=None)

#Output format should be standard fasta file format.
"""
>peak name size: len(seq) ; location: chr start end
ACGTTTTGGTTGTGTCCC
>peak name size: len(seq) ; location: chr start end
ACGTTTTGGTTGTGTCCC
"""
# bed[0][i]=footprint name, bed[1][i] is the chronosome, bed[2][i] is the start, bed[3][i] is the end
for i in range(1, len(bed[0])):
    print(">" + bed[0][i] + " " + "size:" + str(int(bed[3][i])-int(bed[2][i])) + " " + "location:" + " " + str(bed[1][i]) + " " + str(bed[2][i]) + " " + str(bed[3][i]))
    print(genome.fetch(bed[1][i] , start=int(bed[2][i]), end=int(bed[3][i])))

```

Run the above like this: `python fetch_genomic_region.py ${bowtie_index} merged.peaks.bed > merged.peaks.fa`
This will produce a fasta format version of your peak file.

Then use fimo from the meme suite to match the sequences against motifs.

fimo_motifmatch.sh
```bash
#!/bin/bash

#SBATCH -J Fimo
#SBATCH -n 16
#SBATCH -t 01:00:00

motifdb=${1}
fasta=${2}

fimo ${motifdb} ${fasta}

```

To submit to queue: sbatch fimo_motifmatch.sh $Motif_databases/CIS-BP/Homo_sapiens.meme merged.peaks.fa
The database file can be downloaded from the meme webpage

#### Footprinting

Footprinting is used to detect sites in peaks where something is likely to bind. These places in theory appears as a spot of ATAC-seq signal with two shoulders of high signal and a valley with low signal in between.
For footprinting there are two primary tools to use. RGT-Hint and welligton. We will be using wellington since it is easier to integrate with meme and more flexibel than hint.
Note that footprinting is very computationally intense and takes a long time. Do not be surprised if your job takes more than 50 hours to finish.

We will again use the merged.peaks.bed file as an example.
First wellington needs your ATAC-seq signnal to estimate footprints so merge all bam files of a certain condition into one. If you whant to you can use the samples of condtion two as controll in the wellington to get footprints that are bound in condition one but not 2.

Merge bamfiles

bammerge.sh

```bash
#!/bin/bash

#SBATCH -n 2
#SBATCH -t 04:00:00 # This takes awhile since the compression will only use one thread.

files=$(find ./aligned/cond1*.bam -type f)

samtools merge $files > merged.bam
samtools index merged.bam

```

Then footprint with the merged bam file and desired bed file. This might take alot of time.

wellington_footprint.sh

```bash
#!/bin/bash

#SBATCH -J wellington
#SBATCH -n 32
#SBATCH -t 72:00:00

bed=$1
bam=$2
out=${bam%%.*}_footprints

wellington_footprints.py -p 32 ${bed} ${bam} ${out} -sh 7,36,1 -fp 6,41,1 -A -fdr 0.01 -fdriter 100 -fdrlimit -30

```

If wellington is taking to much time and you do not know if it's just getting stuck or something you can try it out first without the -sh and -fp parameters as they are alot more inclusive than the default and therefore slows it down by a factor of about 25.

#### Annotate peaks or footprints to the nerest gene.

One way to guess the effect of a footprint or a peak is to look what genes are closest to it. This can be done with a package called Homer which offers scripts to annoatate peaks to the nersest genes and do a goterm analysis of said genes.

annotatepeaks.sh

```bash
#!/bin/bash

#SBATCH -J wellington
#SBATCH -n 4
#SBATCH -t 01:00:00

bed=$1
name=$2

annotatePeaks.pl ${bed} hg38 -size given -annStats ${name}.annstat.txt -go ${name}_go/ -genomeOntology ${name}_genomeOntology/ > ${name}.annotated_footprints.bed

```














