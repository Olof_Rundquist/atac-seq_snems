##Outline

### Peak calling

```
    1. Call peaks on all samples seperatley with Genrich
    2. Create Consensus peakfiles for each group with peaks occuring in at least 70 % of samples.
    3. Create a union of thses four files andgive it to Julia for differential peak calling.

```

### Footprinting

```
    1. Creat union bam files for each sample group.
    2. Wait for the differntial peaks sets
    3. Call footprints with the union bam files on the peak set produced by Julia with RGT
        Also output bigwig files for vissulization.
    4. Exlude fotprints based on the FOS (DEPTH) of the footprint
        Calculate FOS for all footprints and plot it. Make a resonable cutoff.
        Alternativy base the cutoff on chip-seq data from T-cells.
    5.Match to Motifs
        Hand to Julia
    6.Try out Rgt-hint differential.
    
    ```
    
        