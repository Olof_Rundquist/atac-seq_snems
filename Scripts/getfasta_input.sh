#!/bin/bash -l

files=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/Annotated_footprints/*bed -type f)
Genome=/proj/lassim/x_oloru/genomes/hg38/genome.fa

for i in $files
do
    python ./bin/fetch_genomic_region.py $Genome $i > ${i%%.*}.footprints.fimo.fa
done
