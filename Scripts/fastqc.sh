#!/bin/bash -l

#SBATCH -J fastqc
#SBATCH -t 04:00:00
#SBATCH -n 32

source deactivate
source activate NGS_env

#make the output directory for the reports

#$1 is $ATAC-seq_files/*fastq.gz 
fastqc -t 128 -o $1 $2  

#cd into output directory and run multiqc.
cd $1

multiqc *zip
