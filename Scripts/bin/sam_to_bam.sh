#!/bin/bash -l

#SBATCH -J Sam_to_bam
#SBATCH -n 1
#SBATCH -t 02:00:00
#SBATCH --mem=6144 # 6gb

sam=${1}

samtools view -bS $sam > ${sam%%.sam}.bam
samtools sort -m 6G -n ${sam%%.sam}.bam > ${bam%%.*}.sorted.bam
samtools index ${sam%%.*}.sorted.bam

rm $sam

sbatch ./bin/genrich_run.sh ${bam%%.*}.sorted.bam 
