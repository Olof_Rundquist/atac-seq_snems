#!/bin/bash -l

#cd ../../../aligned/


Patients_activated=$(find P*A_*generich_peaks.bed)
Patients_unactivated=$(find P*U_*generich_peaks.bed)
Controlls_activated=$(find C*A_*generich_peaks.bed)
Controlls_unactivated=$(find C*U_*generich_peaks.bed)

bedtools multiinter -i $Patients_activated > Patients_activated_multiintersect.bed
bedtools multiinter -i $Patients_unactivated > Patients_unactivated_multiintersect.bed
bedtools multiinter -i $Controlls_activated > Controlls_activated_multiintersect.bed
bedtools multiinter -i $Controlls_unactivated > Controlls_unactivated_multiintersect.bed

python ../atac-seq_snems/Scripts/bin/Filter_peaks.py 


for i in *consensus.bed
do 
    #bedtools merge -d 75 -i $i > ${i%%.bed}.merged.bed #Merges all peaks with less than 75bp (=read_length) between each other
    bedtools sort -i $i | bedtools merge -d 75 > ${i%%.bed}.merged.bed #Merges all peaks with less than 75bp (=read_length) between each other
done

#And finnaly take the union of the files
merged=$(find *merged.bed)

cat $merged | bedtools sort | bedtools merge -d 75 > Full_consensus_0.0_peaks.bed


 
