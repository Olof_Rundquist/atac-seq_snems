#!/bin/bash -l

#SBATCH -t 01:00:00
#SBATCH -n 1
#SBATCH --mem=6000

tssenrich --genome hg38 --memory 3 --processes 1 --log tss_scores/${1%%.*}.log.txt $1 > ./tss_scores/${1%%.*}.tss_score.txt
