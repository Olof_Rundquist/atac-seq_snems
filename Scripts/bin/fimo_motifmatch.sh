#!/bin/bash -l

#SBATCH -J Fimo/Centrimo
#SBATCH -n 8
#SBATCH -t 01:00:00

source activate NGS_env

motifdb=${1}
bed=${2}

#mkdir ${3}
cd ${3} # cd directory to the output directory.

name=${bed##/*/}
#Matches motifs
fimo --o ${name%%.*}.match ${motifdb} ${bed}
#Performs enrichment analysis
centrimo --local --ethresh 1000 --o ${name%%.*}.enrichment ${bed} ${motifdb}
