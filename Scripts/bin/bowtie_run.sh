#!/bin/bash

#SBATCH -J bowtie2
#SBATCH -t 01:00:00
#SBATCH -N 1
#SBATCH -n 32

forward=$(echo ${1} | tr " " ",") # Creates comma delimited list of forward read files
reverse=$(echo ${2} | tr " " ",") # Creates comma delimited list of reverse read files
unpaired=$(echo ${3} | tr " " ",") # Creates comma delimited list of unpaired read files
ref_index=${5} # Reference genome.
out=${4}

bowtie2 -p 32 -k 10 --very-sensitive -x $ref_index -1 ${forward} -2 ${reverse} -U ${unpaired} -S ${out}.Aligned.sam --met-file ${out}.bowtie.metrics.txt

sbatch ./bin/sam_to_bam.sh ${out}.Aligned.sam
