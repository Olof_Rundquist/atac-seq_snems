#!/bin/bash
#SBATCH -J wellington # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 48:00:00 # 48 h
#SBATCH -o footprinting-%A-%a.out # Standard output
#SBATCH -e footprinting-%A-%a.err # Standard error

#This script needs a diffrent env since pyDNase is not fully upgraded to python3
source activate pydnase

bed=${1} #Peak file (merged_peaks.bed)
bam=${2} #bam file
out=${bam%%.*}_wellington_footprints # output directory.

#Run per file
mkdir -p ${out}
wellington_footprints.py -p 0 ${bed} ${bam} ${out} -sh 7,36,1 -fp 6,41,1 -A -fdr 0.01 -fdriter 100 -fdrlimit -30

#This did not work.
if [[ ! -f ${out}/*FDR*bed ]] ; then
    echo 'File "${out}/*FDR*bed" is not there, aborting.'
    exit 0
fi

#parent_dir=${bam%%/aligned/*}/aligned
#mkdir -p ${parent_dir}/Annotated_footprints
#sbatch ./bin/annotate_homer.sh ${out}/*FDR*bed
