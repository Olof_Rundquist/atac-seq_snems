#!/bin/bash
#SBATCH -J MACS2 # A single job name for the array
#SBATCH -n 16 # Processes
#SBATCH -t 00:45:00 # 1 h
#SBATCH -o MACS2-%A-%a.out # Standard output
#SBATCH -e MACS2-%A-%a.err # Standard error

bam=${1}

mkdir ${bam%%.*}_macs_peaks # macs will fail if the outdir does not excist.
macs2 callpeak --format BAMPE -t ${bam} --outdir ${bam%%.*}_macs_peaks -n ${bam%%.*} --nomodel
#macs2 callpeak --format BAMPE --broad -t ${bam} --outdir ${bam%%.*}_macs_peaks -B --SPMR -n ${bam%%.*} --nomodel --shift -100 --extsize 200


cd ${bam%%.*}_macs_peaks

bedtools merge -i ${bam%%.*}_peaks.broadPeak
