#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 16:06:01 2019

@author: x_oloru
"""

import pandas as pd
from sys import argv
from os import listdir

#First run bedtools multiniter acrooss all files in X group.
for i in [x for x in listdir() if "multiintersect.bed" in x]:
    df=pd.read_table(i, header=None)
    #Drop unecessary columns
    df=df.iloc[:, 0:4]
    #Number of samples
    sample_number=max(df[3])
    #Drop areas supported by less than 70% of samples in group.
    df=df[df[3] >= 0 * int(sample_number)]
    #Done
    df.to_csv(i.split(".")[0] + ".consensus.bed", index=None, header=None, sep="\t")
