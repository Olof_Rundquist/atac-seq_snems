#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH --mem=24566 # 24gb Uses like 18gb

bam=$1
out=${bam%%.bam}.generich_peaks.bed

Genrich -t $bam -o $out -j -y -r -v -e chrM 
