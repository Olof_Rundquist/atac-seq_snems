#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 32
#SBATCH -J Trim_galore

files=${1} # space delimted list of input files
name=${2}

mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/trimmed_galore/${name}
trim_galore -o /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/trimmed_galore/${name} --nextera --fastqc --paired --retain_unpaired ${files} 
