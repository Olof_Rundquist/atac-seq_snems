#!/bin/bash
#SBATCH -J FeatureCounts # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 02:00:00 # 1 h
#SBATCH -o Feature-%A-%a.out # Standard output
#SBATCH -e Feature-%A-%a.err # Standard error

#Activate the conda enviroment. 
#source activate NGS_env

files=$(find *Aligned.sorted.bam -type f)
annotation=Full_consensus_0.70_peaks.saf
output=Full_consensus_0.70_peaks.count_matrix.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files} 2> Counting_log.txt 
