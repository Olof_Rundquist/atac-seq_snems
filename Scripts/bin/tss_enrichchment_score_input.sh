#!/bin/bash -l

for i in *sorted.rmdup.bam
do
    sbatch tss_enrichment_score.sh $i
done
