#!/bin/bash -l

#Shift the bam file. 
#This is taken from the original ATAC-seq papper.
samtools sort $bam | bedtools bamtobed -bedpe | awk -v OFS="\t" '{if($9=="+"){print $1,$2+4,$6+4}else if($9=="-"){print $1,$2-5,$6-5}}' | bedtools bedtobam -g hg38 > ${bam%%.bam}.shiffted.bam

