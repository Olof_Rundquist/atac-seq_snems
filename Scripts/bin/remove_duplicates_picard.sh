#!/bin/bash -l

#SBATCH -J Picard-rmdup
#SBATCH -n 2
#SBATCH -t 02:00:00

#For unknow reasons samtools truncate the file when removing duplicates.
#Therfore we are now using picard instead.
bam=${1}
picard MarkDuplicates I=${bam} M=${bam%%.*}.duplicate.metrics.txt O=${bam%%.*}.sorted.rmdup.bam REMOVE_DUPLICATES=true

samtools index ${bam%%.*}.sorted.rmdup.bam

