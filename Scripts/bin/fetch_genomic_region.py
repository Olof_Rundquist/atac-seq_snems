#!/usr/bin/env python

import pysam
import pandas as pd
import sys

genome=sys.argv[1]
bed=sys.argv[2]

genome=pysam.FastaFile(sys.argv[1])
bed=pd.read_table(sys.argv[2], header=None)

#Output format should be standard fasta file format.
"""
>peak name size: len(seq) ; location: chr start end
ACGTTTTGGTTGTGTCCC
>peak name size: len(seq) ; location: chr start end
ACGTTTTGGTTGTGTCCC
"""
# bed[0][i]=footprint name, bed[1][i] is the chronosome, bed[2][i] is the start, bed[3][i] is the end
for i in range(1, len(bed[0])): 
    print(">" + bed[0][i] + " " + "size:" + str(int(bed[3][i])-int(bed[2][i])) + " " + "location:" + " " + str(bed[1][i]) + " " + str(bed[2][i]) + " " + str(bed[3][i]))
    print(genome.fetch(bed[1][i] , start=int(bed[2][i]), end=int(bed[3][i])))
