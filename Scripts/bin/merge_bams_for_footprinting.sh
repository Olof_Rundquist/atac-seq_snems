#!/bin/bash -l

#SBATCH -N 1
#SBATCH -n 32
#SBATCH -t 04:00:00

Activated_Patients=$(find ../../../aligned/P*A_*sorted*bam)
UnActivated_Patients=$(find ../../../aligned/P*U_*sorted*bam)
Activated_Controlls=$(find ../../../aligned/C*A_*sorted*bam)
UnActivated_Controlls=$(find ../../../aligned/C*U_*sorted*bam)

#samtools merge -n --threads 32 ../../../aligned/P_All_A.merged.bam $Activated_Patients 
#samtools merge -n --threads 32 ../../../aligned/P_All_U.merged.bam $UnActivated_Patients 
#samtools merge -n --threads 32 ../../../aligned/C_All_A.merged.bam $Activated_Controlls 
samtools merge -n --threads 32 ../../../aligned/C_All_U.merged.bam $UnActivated_Controlls 





