#!/bin/bash -l

samtools view -h ${1} | awk '{if($3 != "chrM" && $3 != "chrUn"){print $0}}' | samtools view -Sb - > ${1%%.*}.sorted.rmdup.filtered.bam

