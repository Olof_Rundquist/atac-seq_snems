#!/bin/bash -l

for i in ./*vs*/NA_peaks.narrowPeak ;
do
    awk -v OFS = "\t" 'BEGIN { print "PeakID", "Chr", "Start", "End", "Strand" } { print "Peak_"NR, $1, $2, $3, "." }' ${i} > {$i}.saf
done

awk 'OFS="\t" {print $1"."$2+1"."$3, $1, $2+1, $3, "."}' in.narrowPeak > out.saf
