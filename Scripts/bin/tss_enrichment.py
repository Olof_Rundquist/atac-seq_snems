#!/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 13:06:21 2019

@author: x_oloru
"""

from os import listdir
import tssenrich

bams=[x for x in listdir() if "filtered.bam" in x]
bams=[x for x in bams if ".bai" not in x]

with open("Tss_scores.csv", "w") as f:
    for i in bams:
        sample_name=i.split(".")[0]
        score=tssenrich.tss_enrichment(bam_file_path = i, genome="hg38", memory_gb=2)
        line=sample_name + "," + str(score) + "\n"
        f.write(line)

