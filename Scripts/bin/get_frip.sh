#!/bin/bash -l

frip=$(grep -R "Successfully assigned fragments" Counting_log.txt) # Get the line
frip=$(cut -d "(" -f 2 <<< "$frip") # Get the interesting half
frip=$(cut -d ")" -f 1 <<< "$frip") # get the precentage
# Make into list
arrfrip=($frip)

# Now get the sample names
samples=$(grep -R "Process BAM file" Counting_log.txt) # Get the line
samples=$(cut -d "." -f 1 <<< "$samples") # Get the interesting half
samples=$(cut -d " " -f 5 <<< "$samples") # get the precentage
#Make into list
arrsamples=($samples)

#Print to standard out as array
for index in ${!arrsamples[*]} 
do
    echo ${arrsamples[$index]}","${arrfrip[$index]}
done
