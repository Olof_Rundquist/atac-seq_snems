#!/bin/bash -l

#SBATCH -j tssenrich
#SBATCH -n 16
#SBATCH --memory=96000
#SBATCH -t 08:00:00

tssenrich --genome hg38 --log Tss_log.txt --memory 96 --processes 16 *filtered.bam > Tss_enrichment_scores.txt
