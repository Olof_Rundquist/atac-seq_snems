#!/usr/bin/env python

import pandas as pd
import seaborn as sb
from matplotlib import pyplot as plt

#FRIP
frip=pd.read_csv("frip_score.csv", sep=",", header=None)
frip[1] = frip[1].str.strip("%") 
frip[1] = pd.to_numeric(frip[1]) 

plt.figure(figsize=(18.5, 10))
frip_bars=sb.barplot(x=frip[0], y=frip[1]) 
plt.ylabel("FRIP(%)")

for item in frip_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("frip_bars.svg") 

#Number of peaks
peaks=pd.read_csv("All_scores.txt", sep=" ", header=None)

plt.figure(figsize=(18.5, 10))
peaks_bars=sb.barplot(x=peaks[0], y=peaks[1]) 
plt.ylabel("Number of Peaks")

for item in peaks_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("peaks_bars.svg") 

#tss
tss=pd.read_csv("All_scores.txt", sep=" ", header=None)

plt.figure(figsize=(18.5, 10))
tss_bars=sb.barplot(x=tss[0], y=tss[1]) 
plt.ylabel("TSS Score")

for item in tss_bars.get_xticklabels():                       
    item.set_rotation(90)

plt.savefig("Tss_bars.svg") 
