#!/bin/bash -l 

#SBATCH -n 8
#SBATCH -t 01:00:00

samtools sort --threads 8 -n $1 > ${1%%.bam}.sorted.bam

sbatch ./bin/genrich_run.sh ${1%%.bam}.sorted.bam
