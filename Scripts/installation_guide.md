## Instalation Guide

1. Load the Python/3.6.3-anaconda-5.0.1-nsc1

2. Configure channels
"""
conda --add channels conda-forge
conda --add channels Bioconda
"""
3. Install the packages.
Create the ngs-env
"""
conde create -n NGS_env bowtie samtools bedtools picard subread trim-galore homer meme macs2 
"""
Create the pyDnase env. It is not compatible with the rest so give it an env of it's own.
"""
conda create -n pydnase pydnase
"""

4. Install python pkgs
Exit the env and install pysam. Needed to get the fasta sequence of your peaks.
"""
pip install --user pysam
"""

5. Should be evrything. Rember to activate the correct env before you run each script.