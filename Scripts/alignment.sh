
#!/bin/bash -l 

#Runs the alignment and sorts the files afterwards.

#Find The sample directories
samples=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/trimmed_galore/C* /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/trimmed_galore/P* -prune -type d)
ref_genome=/proj/lassim/users/x_oloru/ATAC-seq_data/bowtie2-2.3.4/indexes/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index
# This finds the forward and reverse files and gives them to bowtie. Write in the samtolls stuff intpo bowtie!
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned
for i in $samples
do
    f=$(find $i/*val_1.fq.gz -type f) # Forward
    r=$(find $i/*val_2.fq.gz -type f) # Reverse
    unp=$(find $i/*unpaired*gz -type f) # unpaired 
    sbatch ./bin/bowtie_run.sh "${f}" "${r}" "${unp}" /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/${i##/*/} ${ref_genome}
done
