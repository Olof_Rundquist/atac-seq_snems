#!/bin/bash -l

samples=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/*sorted.rmdup.bam -type f)

for i in $samples 
do 
    sbatch ./bin/filter_out_chrM.sh ${i}
done
