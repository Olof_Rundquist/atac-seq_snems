#!/bin/bash -l

#The bed files
array1=($(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/consensus_filtered_peaks/*size_*110bp*bed -type f | sort)) 

#The bam files
array2=($(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/*rmdup.filtered.bam -type f | sort))

length=${#array1[@]}

for ((i=0;i<=$length;i++)) # ; do echo -e "${array1[$i]} ${array2[$i]}"; done
do
    sbatch ./bin/wellington_footprinting_run.sh ${array1[$i]} ${array2[$i]}
done

