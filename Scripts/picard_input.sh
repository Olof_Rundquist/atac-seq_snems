#!/bin/bash -l

samples=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/*sorted.bam -type f)

for i in $samples 
do 
    sbatch ./bin/remove_duplicates_picard.sh ${i}
done
