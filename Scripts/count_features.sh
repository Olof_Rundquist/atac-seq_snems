#!/bin/bash
#SBATCH -J FeatureCounts # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 02:00:00 # 1 h
#SBATCH -o Feature-%A-%a.out # Standard output
#SBATCH -e Feature-%A-%a.err # Standard error

#Activate the conda enviroment. 
#source activate NGS_env

files=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/*rmdup.filtered.bam -type f)
annotation=peaks.saf
output=count_matrix.tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files}
