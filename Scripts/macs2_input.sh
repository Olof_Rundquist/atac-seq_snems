#!/bin/bash -l

samples=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/*.sorted.rmdup.filtered.bam -type f)

for i in $samples 
do 
    sbatch ./bin/macs2_run.sh ${i}
done
