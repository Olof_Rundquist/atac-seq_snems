## README for scripts

### Run scripts in this order:

Detailed instructions can be found in tutorial.

1. Quality controll:
fastqc.sh

2. Trim adaptors and bad reads.
trim_galore_run.sh

3. Align to reference
alignment.sh

4. File conversion to bam and deduplication of reads.
picard_input.sh

5. Remove the mitocondria chr
filter_chrm_out_input.sh 

6. Call Peaks # Edit to remove broad call version.
macs2_input.sh

7. Merge peaks to count feautures
merge_bed.sh

8. Count reads in peaks
count_features.sh

9. Limma analysis: Julia
See Rscript

#### Note the following is only a suggestion and very outdated. See Tutorial files

The limma analysis should produce three to five files:
Peaks with significant diffrences between activated and uncativate:
    One for patients and one for controlls.

Peaks with significant diffrence between controlls and patiensts:
    One for activated and one for unactivated (both optional)
    One for dynamic reponse (e.g (pA + pU)-(cA + cU))

Then take the union of these intresting peaks for the upcomming analysis and match and split to get results at the end.
Alternivly do not do that and run the bellowe for all five files. That might get a bit chaotic though.

#### Get the fasta sequence of the peaks.
getfasta_input.sh 
    Just give the merged peak file to the fetch_genomic_region.py script along with the geneome and it should work.

#### Footprint and map motfis. Also map the peak to the nearest gene. 
The scripts do a bit more than needed so they will be modified. Instruction:
Wellington:
First Wellington will not run on sequences shorter than 110 bp and will crash. 
Either filter out theses sequences or change the statrt and end of region if the diffrences is below 110 bp.
Wellington requires it's own virual env with pydnase due to compatabilty issues with python3. See tutorial document
Run wellington with the following input:
"""
mkdir ${out} # make the output dir.
wellington_footprints.py -p 0 ${bed} ${bam} ${out} -sh 7,36,1 -fp 6,41,1 -A -fdr 0.01 -fdriter 100 -fdrlimit -30
"""
${bed} = The bed file from the limma. Note, not the fasta file!
${bam} = Merged bam files from the diffrent condtions. Use samtools merge to merge all bam files from the below conditions. Index the merged files afterwards. Note this takes like 3h.
    1. All unstimulated controlls merged.
    2. All stimulated controlls merged
    3. All unstimulated  merged
    4. All stimulated controlls merged
Run for all four bam files.
${out} = Output directory. Needs to be created before wellington runs. It has to be empty.
Note the welligton takes around 55h on 16 cores for 50000 peaks. Give it ample time.

This should get you four footprint files to compare between. 

Alterintivly use welligton bootstrap to compare between two conditions, in theory it should give the diffentially bound sites between the two conditions. 
However this has never worked in testing and the script has a tendancy to run forever, return no output and say nothing about errors.
"""
wellington_footprints.py ${treatment_bam} ${controls_bam} ${bed} ${treat_out} ${control_out} -A -fdr 0.01 -fdriter 100 -fdrlimit -30
"""

Anyway now you have footprints.

#### Fimo/Centrimo:
run:
"""
mkdir ${outdir}
./bin/fimo_motifmatch.sh ${db} ${fasta} ${outdir}
"""
${db} = database. I would recommend cis-bp human. I will link in all I have.
${fasta} = fasta sequences from the bed file.
${outdir} = Output dir. 

This makes a map of all motifs present in the peaks.

#### Mapping the peaks to genes:
This is done with annotatePeaks.pl from HOMER.
run: 
"""
annotatePeaks.pl ${bed} hg38 -size given -annStats ${outdir}/annstat.txt -go ${outdir}/go > ${outdir}/annotated_peaks.bed
"""
${bed} = The bed file from the limma. Note, not the fasta file.
This might be more pratical to do on the footprints to specfically target it to the conditions.
This also does a go-term analysis of the genes annotated to the peaks/footprints.

#### How to get results.
Now overlap the motif locations from fimo with the footprints. 
I recommend "bedtools intersect" for this.
This will intersect the two bedfiles where they overlap by at least one base pair. 
I think it can also output the ones that do not intersect.

Beyond that I am not an expert. 
I can recommend to do enrichmnnts and/or Gene set enrichments for the binding 
TFs and the genes annotated to the footprints. 
I guess the networks also commes in here.
By the binding TFs i refere the tfs that bind the motifs that overlap with the footprints.

Note when writing this I do realise that by taking the union of the limma peaks it might make the limma pointless. 
Make your own judgmnet. 
If taking the union of limma peaks creates a peak file of very similar length to the original union peak file from all samples
then it may be better to take the peaks from limma from each comparision and do footprinting with the two groups that were compared for each.
This will require you to run the above scripts for many more files and will make the iterpretation more difficult.
Still that might provide better results.


For instalation guide see the instalation guide. It will also be in the Tutorial. The tutorial will end at step 9 since I can't really say much after that.
