#!/bin/bash -l
#SBATCH -J bed-merge # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 4 # Processes
#SBATCH -t 00:30:00 
#SBATCH -o bedmerge-%A-%a.out # Standard output
#SBATCH -e bedmerge-%A-%a.err # Standard error

files=$(find ../aligned/*narrowPeak)

cat $files | bedtools sort | bedtools merge > merged.peaks.bed

