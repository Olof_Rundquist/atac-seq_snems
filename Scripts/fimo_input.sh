#!/bin/bash -l

#Make the output directories.
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/cis-bp
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/hocomocco
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/jaspar
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/encode
mkdir /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/uniprobe

#Motif databases
cis_bp=/proj/lassim/users/x_oloru/motif_databases/CIS-BP/Homo_sapiens.meme
hoccomoco=/proj/lassim/users/x_oloru/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
jaspar=/proj/lassim/users/x_oloru/motif_databases/JASPAR/JASPAR2018_CORE_vertebrates_non-redundant.meme
encode=/proj/lassim/users/x_oloru/rgtdata/motifs/encode.meme
uniprobe=/proj/lassim/users/x_oloru/motif_databases/MOUSE/uniprobe_mouse.meme

files=$(find /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/Annotated_footprints/*.fimo.fa -type f)

for i in $files
do  
    sbatch ./bin/fimo_motifmatch.sh ${cis_bp} ${i} /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/cis-bp
    #sbatch ./bin/fimo_motifmatch.sh ${hocomocco} ${i} /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/hocomocco
    #sbatch ./bin/fimo_motifmatch.sh ${jaspar} ${i} /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/jaspar
    #sbatch ./bin/fimo_motifmatch.sh ${encode} ${i} /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/encode
    #sbatch ./bin/fimo_motifmatch.sh ${uniprobe} ${i} /proj/lassim/users/x_oloru/ATAC-seq_snems/rawdata/aligned/motif_matching/uniprobe
done
#Just do cis-bp for now. Run the other four at request.
