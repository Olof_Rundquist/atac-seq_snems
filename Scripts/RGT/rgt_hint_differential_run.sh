#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 8 # Rgt-hint differential can not efficiently use more than 8 threads. I have tested this.
#SBATCH -t 24:00:00
#SBATCH --mem=90000 # memory usage in mb

reads1=$1
reads2=$2
mpbs1=$3
mpbs2=$4

out=${5}
rgt-hint differential --output-profiles --bc --organism hg38 --mpbs-file1 $mpbs1 --mpbs-file2 $mpbs2 --reads-file1 $reads1 --reads-file2 $reads2 --condition1 UAct --condition2 Act --nc 8 --output-location ./${out%%U*.bed}_all_peaks_differential
