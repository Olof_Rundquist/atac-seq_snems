#!/bin/bash -l

bed=$1

for i in ../*merged.bam
do
    sbatch rgt_hint_run.sh $i $bed 
    sbatch rgt_hint_tracks.sh $i $bed # makes tracks for visulaizations with IGV or Gviz
done 
