#!/bin/bash -l

#SBATCH -J rgt-hint
#SBATCH -n 1
#SBATCH -t 08:00:00
#SBATCH --mem=15000

bam=$1
bed=$2

out=${1##../} # Crops input to only the file name.
rgt-hint footprinting --organism hg38 --atac-seq --paired-end --output-prefix=${out%%.*}_rgt_footprints ${bam} ${bed} 
