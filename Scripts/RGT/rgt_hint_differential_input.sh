#!/bin/bash -l

#Controll agianst patients
sbatch rgt_hint_differential_run.sh ../P_All_A.merged.bam ../C_All_A.merged.bam ./match/P_All_A.rgt_footprints.mpbs.bed ./match/C_All_A.rgt_footprints.mpbs.bed P_vs_C_Act 
sbatch rgt_hint_differential_run.sh ../C_All_U.merged.bam ../P_All_U.merged.bam ./match/P_All_U.rgt_footprints.mpbs.bed ./match/C_All_U.rgt_footprints.mpbs.bed P_vs_C_UAct

#Activated vs unactivated
sbatch rgt_hint_differential_run.sh ../P_All_U.merged.bam ../P_All_A.merged.bam ./match/P_All_U.rgt_footprints.mpbs.bed ./match/P_All_A.rgt_footprints.mpbs.bed Act_vs_UAct_P
sbatch rgt_hint_differential_run.sh ../C_All_U.merged.bam ../C_All_A.merged.bam ./match/C_All_U.rgt_footprints.mpbs.bed ./match/C_All_A.rgt_footprints.mpbs.bed ACT_vs_UAct_C
