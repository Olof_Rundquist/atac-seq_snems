#!/bin/bash -l

#SBATCH -J rgt-enrichment
#SBATCH -n 1
#SBATCH -t 02:00:00

background=$1
fp=$2
rgt-motifanalysis enrichment --organism hg38 $background $fp
