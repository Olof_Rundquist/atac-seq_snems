#!/bin/bash -l

background=$(find ./match/random*)

#Enrichment against random
for i in *rgt_footprints.bed
do
    sbatch rgt_motifanalysis_enrichment_run.sh $background $i
done

#Enrichment agianst controll
sbatch rgt_motifanalysis_enrichment_against_controll.sh P_All_U.rgt_footprints.bed P_All_A.rgt_footprints.bed
sbatch rgt_motifanalysis_enrichment_against_controll.sh C_All_U.rgt_footprints.bed C_All_A.rgt_footprints.bed

