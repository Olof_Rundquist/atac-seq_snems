#!/bin/bash -l

#SBATCH -J rgt
#SBATCH -n 1
#SBATCH -t 04:00:00

rgt-motifanalysis matching --organism hg38 --rand-proportion 10 --input-files $1
