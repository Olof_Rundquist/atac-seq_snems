#!/bin/bash -l

#SBATCH -J rgt-enrichment
#SBATCH -n 1
#SBATCH -t 02:00:00

background=$1
activation=$2
rgt-motifanalysis enrichment --organism hg38 --output-location ./enrichment_against_controll/ $background $activation
