This directory contains a summary of the quality
controlls performed post peak calling to check the 
signal to noise ratio in the samples. How these checks were perfomed is outlined at the end of the tutorial.

This quality checks is summarised in the excel table.

## Fragment size 
Fragment size is a measure of library complexity and defines the amount 
of information one can expect from each sample.

In the fragment size plots one wishes to see two things.
```
        1. In the linear plot, a peak at less than 50bp for short inserts from open chromatin.
        2. In the log plot, a good nuclesome distribution with peaks at 147, 300, 450 etc base pairs.
```
A lack of short fragments means that there is limited information on open regions of the chromatin.

A lack of nuclesome peaks implies denaturation of the chromatin during fragmentation leading to random fragmentation of the DNA.

If any of these peaks were missing the sample was considered as having failed 
this quality check. 

Good Fragment size distribution:
![Inline image](Fragment_size_plots/C12A_linear_fragment_size.png)

Bad Fragment size distribution:
![Inline image](Fragment_size_plots/P23A_linear_fragment_size.png)


## Tss score
The Tss score is a measurment of signal to noise ratio. In ATAC-seq the largest peaks in activley transcribed genes is expected to occur at the transcription start site  where the transcription initation complex is formed. 
Tss score is simply calculated by taking the average of the signal over Tss divided by the signal at 3000bp away from Tss. 3000bp from Tss is considered the promoter gene boundary. 

Ideal Tss score varies with the assembly of the used reference genome. See the encode webpage. For Hg38 a value above seven is ideal while above five can be considered as acceptable.

In general to pass the quality check as a whole a sample needed to have a Tss above five and pass the fragment size check. 
A few exceptions were made to this rule for samples that had a tss score below 5 but very good fragment size distribution.

TSS plot
![Inline image](Tss_bars.png)
